<?php include 'header.php' ?>
	
	<div class="btn-wrapper">
		<div class="container cf head-no-pad">
			
			<a href="javacript:;" class="btn remove-image cf"><span class="icon-x">&times;</span><span>Remove Image</span></a>
			
			<a href="javacript:;" class="btn print-recipe cf">
				<span class="icon-wrapper">
					<span class="icon-print"></span>
				</span>
				<span class="caption">
					<span>Print Recipe</span>
				</span>
			</a>
			
		</div>
	</div>

	<div class="container bordered cf">

		<div class="namnam-box"></div>

		<div class="content cf">

  			<div class="imgholder" style="background-image:url(assets/img/sample-img.jpg);"></div>
  			<span class="recipe-name">Beef Caldereta ala NamNam Tomato</span>
  			
  			<div class="text-editor-wrap">
  			<!--
			note.
  			-->
			<table class="ingre">
				<tr><th colspan='3'>What You Need</th></tr>

				<tr>
					<td>2</td>
					<td>lbs.</td>
					<td>beef, cut into cubes</td>
				</tr>

				<tr>
					<td>1</td>
					<td>cup</td>
					<td>vegetable oil</td>
				</tr>

				<tr>
					<td>3</td>
					<td>cloves</td>
					<td>garlic,chopped</td>
				</tr>

				<tr>
					<td>1</td>
					<td></td>
					<td>onion, chopped</td>
				</tr>

				<tr>
					<td>1</td>
					<td>tsp.</td>
					<td>chili flakes</td>
				</tr>

				<tr>
					<td>4</td>
					<td>cup</td>
					<td>water</td>
				</tr>

				<tr>
					<td>1</td>
					<td>cup</td>
					<td>tomato sauce</td>
				</tr>

				<tr>
					<td>1/2</td>
					<td>cup</td>
					<td>liver spread</td>
				</tr>

				<tr>
					<td>3</td>
					<td></td>
					<td>bay leaf</td>
				</tr>

				<tr>
					<td>2</td>
					<td>cup</td>
					<td>potato quartered</td>
				</tr>

				<tr>
					<td>2</td>
					<td>cup</td>
					<td>carrots, sliced</td>
				</tr>

				<tr>
					<td>2/3</td>
					<td>cup</td>
					<td>green olives</td>
				</tr>

				<tr>
					<td>1</td>
					<td></td>
					<td>red bell pepper, sliced</td>
				</tr>

				<tr>
					<td>1</td>
					<td></td>
					<td>green pepper, sliced</td>
				</tr>

				<tr>
					<td>1</td>
					<td>pack</td>
					<td>Lucky Me Namnam Tomato</td>
				</tr>


			</table>
			
			<table class="steps">
				<tr><th colspan='3'>How to Cook</th></tr>
				
				<tr>
					<td>

				1. In a large pan, heat oil. Fry the potatoes and carrots until color turns light brown. Remove and set aside.
				<br/><br/>
				2. Add beef to the pan; stir-fry for a few minutes until lightly brown. Remove and set aside.
				<br/><br/>
				3. In the same pan, sauté the garlic and onions until translucent. Add chili flakes. Return beef and accumulated juices to pan. Stir-fry for a  few minutes. 
				<br/><br/>
				4. Add water, tomato sauce, liver spread and bay leaf; stir to combine. Bring to a boil then simmer, about an hour or until meat tender.
				<br/><br/>
				5. Add potatoes, carrots and green olives; simmer for another 6 to 8 minutes. Season salt and pepper to taste. 
				<br/><br/>
				Add bell peppers, cook for additional 2 minutes.
				Transfer to a platter. Serve hot and Enjoy your Kaldereta.

					</td>
				<tr>

			</table>

  			<!--end of text-editor-wrap-->	
  			</div>
  		
  		<!--end of content-->
  		</div>

  		<div class="copyright">
  			Copyright 2014, Monde Nissin Corporation. All rights reserved.
  		</div>

  	</div>

<?php include 'footer.php' ?>
