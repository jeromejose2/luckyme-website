
  <script src="assets/vendors/jquery/jquery.js"></script>

  <!-- build:js assets/js/app.min.js -->
  <script src="assets/js/app.js"></script>
  <!-- /build -->

  <div class="sticky-link">	
	  	<div class="container clearpads">
	  		<a href="">Back to <b>Recipes Page</b></a>
	  	</div>
  	</div>
</body>
</html>
