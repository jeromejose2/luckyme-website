
var r_all = 1;
var r_org = 1;
var r_tom = 1;

function getRecipeCount() {
    $.ajax({
         type: "POST",
         url: "ajax/recipecount",
         dataType: "json",
         data: {category: $('#filter_select').val()},
         success : function(data) {
            r_all = data['all'];
            r_org = data['org'];
            r_tom = data['tom'];
            init_paginate(1);
            changePage(1);
        }
    });
}

$(document).ready(function() {
    ajaxInitRecipe('allrecipes', 1);
    var recipe_id = $("#recipe_var").data('id');
    if(recipe_id != 0)
    {
        lytebox.load({
            url: "inner-recipe-popup.php",
            bgClose: !1,
            data: { id: recipe_id}//Added by DM-Leonard
        })
    }
});

//youtube

//load youtube iframe api
var tag = document.createElement('script');
tag.src = "http://www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//object to hold all players on the page
var players = {};

function onYouTubePlayerAPIReady() {

     $(document).ready(function() {

         $('iframe').each(function(event) {

            var iframeID = $(this).attr('id');

            players[iframeID] = new YT.Player(iframeID);

        });

    });
}

function playYouTubeVideo(iframeID) {

    players[iframeID].playVideo();

}

function pauseYouTubeVideo(iframeID) {

    players[iframeID].pauseVideo();
}

function pauseAllYouTubeVideo() {
    $.each(players, function(i, val) {
        pauseYouTubeVideo(i)
    });

}
    
//youtube end

function fb_sharer(url, windowName)
{
    var left = (screen.width/2)-(500/2);
    var top = (screen.height/2)-(300/2);
    window.open(url, windowName, "top="+ top + ", left=" + left + ", height=300, width=500");
}

$('#musicBtn').click(function() {
    var music = document.getElementById('musicPlayer');

    if(music.paused)
    {
        music.play()
    }
    else
    {
        music.pause()
    }
});


function init_paginate(page_number)
{
    filter = getActiveFilter();
    if(filter == 'allrecipes')
    {
        count = r_all;
    }
    else if(filter == 'originalrecipes')
    {
        count = r_org;
    }
    else if(filter == 'tomatorecipes')
    {
        count = r_tom;
    }

    count_page = Math.ceil(count/8);

    if(count_page < 5)
    {
        for(i = count_page+1; i <= 5; i++ )
        {
            $("#p_"+i).attr('class', 'p_num p_disabled' );
        }
        $("#p_next").attr('class', 'p_disabled');
    }
    if(page_number != 'none')
    {
        $("#p_"+page_number).attr('class', 'p_num active');
    }
}


$(".p_num").click(function(e) {
    e.preventDefault();
    var page = parseInt($(this).text());
    if($(this).attr('class') != 'p_num p_disabled')
    {
        if($(this).text() != getActivePage())
        {
            ajaxRecipe(getActiveFilter(), page);
            changePage(page);
        }
    }
});


$("#p_prev").click(function(e) {
    e.preventDefault();
    var page = parseInt(getActivePage());
    if($(this).attr('class') != 'p_disabled' )
    {
        if(page > 5)
        {
            $(".p_num").each(function(index) {
                var text = $(this).text();
                $(this).text(parseInt(text) - parseInt(5));
            });
            changePage(page-5);
            ajaxRecipe(getActiveFilter(), page-5);
        }
    }
    else
    {
        changePage(1);
        ajaxRecipe(getActiveFilter(), 1);
    }
});

$("#p_next").click(function(e) {
    e.preventDefault();
    var page = parseInt(getActivePage());
    var filter = getActiveFilter();
    var count;

    if($(this).attr('class') != 'p_disabled')
    {
        if(filter == 'allrecipes')
        {
            count = r_all;
        }
        else if(filter == 'originalrecipes')
        {
            count = r_org;
        }
        else if(filter == 'tomatorecipes')
        {
            count = r_tom;
        }
        $(".p_num").each(function(index) {
            var text = $(this).text();
            $(this).text(parseInt(text) + parseInt(5));
        });
        if(page+5 <= Math.ceil(count/8))
        {
            changePage(page + 5);
            ajaxRecipe(getActiveFilter(), page);
        }
        else
        {
            changePage(Math.ceil(count/8));
            ajaxRecipe(getActiveFilter(), Math.ceil(count/8));
        }
    }
});

$("#filter_select").change(function() {
    var filter = getActiveFilter();
    
    //refresh the recipe count for each tab
    getRecipeCount();
    
    ajaxRecipe(filter, 1);
    if(filter == 'allrecipes')
    {
        $(".a-tab-nav").attr('class', 'a-tab-nav');
        $("#a_all").attr('class', 'a-tab-nav active');
    }
    else if(filter == 'originalrecipes')
    {
        $(".a-tab-nav").attr('class', 'a-tab-nav');
        $("#a_org").attr('class', 'a-tab-nav active');
    }
    else if(filter == 'tomatorecipes')
    {
        $(".a-tab-nav").attr('class', 'a-tab-nav');
        $("#a_tomato").attr('class', 'a-tab-nav active');
    }
    refreshPagination()
});

$(".a-tab-nav").click(function() {
    $(".a-tab-nav").attr('class', 'a-tab-nav');
    $(this).attr('class', 'a-tab-nav active');
    if($(this).text() == 'All Recipes')
    {
        ajaxRecipe('allrecipes', 1);
    }
    else if($(this).text() == 'Original Recipes')
    {
        ajaxRecipe('originalrecipes', 1);
    }
    else if($(this).text() == 'Tomato Recipes')
    {
        ajaxRecipe('tomatorecipes', 1);
    }
    refreshPagination()
});

function refreshPagination()
{
    changePage(1);
    init_paginate(1);
}

function getActiveFilter()
{
    if($("#a_all").attr('class') == 'a-tab-nav active')
    {
        return 'allrecipes';
    }
    else if($("#a_org").attr('class') == 'a-tab-nav active')
    {
        return 'originalrecipes';
    }
    else if($("#a_tomato").attr('class') == 'a-tab-nav active')
    {
        return 'tomatorecipes';
    }

}

function getActivePage()
{
    if($("#p_1").attr('class') == 'p_num active')
    {
        return $("#p_1").text();
    }
    else if($("#p_2").attr('class') == 'p_num active')
    {
        return $("#p_2").text();
    }
    else if($("#p_3").attr('class') == 'p_num active')
    {
        return $("#p_3").text();
    }
    else if($("#p_4").attr('class') == 'p_num active')
    {
        return $("#p_4").text();
    }
    else if($("#p_5").attr('class') == 'p_num active')
    {
        return $("#p_5").text();
    }

}



function changePage(page)
{
    $(".p_num").attr('class', 'p_num');
    var filter = getActiveFilter();
    if(filter == 'allrecipes')
    {
        count = r_all;
    }
    else if(filter == 'originalrecipes')
    {
        count = r_org;
    }
    else if(filter == 'tomatorecipes')
    {
        count = r_tom;
    }
    var count_page = Math.ceil(count/8);
    if(count_page > 5)
    {
        $("#p_next").attr('class', '');
    }
    else
    {
        $("#p_next").attr('class', 'p_disabled');
    }
    if(page > 5)
    {
        $("#p_prev").attr('class', '');
    }
    else
    {
        $("#p_prev").attr('class', 'p_disabled');
    }
    $(".p_num").each(function(index) {
        if($(this).text() == page)
        {
            $(this).attr('class', 'p_num active');
        }
        if($(this).text() > count_page)
        {

            $(this).attr('class', 'p_num p_disabled');
            $("#p_next").attr('class', 'p_disabled');
        }
    });
    $("#p_"+page).attr('class', 'p_num active');
}

function ajaxRecipe(r_filter, r_page)
{
    //getRecipeCount();
    preload = $("#preload").data('preload');
    for(i = 1; i <= 8; i++)
    {
        $("#recipe" + i).html('');
    }
    $("#recipe" + 1).html('<img src="' + preload + '" class="true-center preloader">');
    var r_category = $("#filter_select").val();
    $.ajax({
         type: "POST",
         url: "ajax/recipe",
         dataType: "json",
         data: { filter: r_filter, page: r_page, category: r_category },
         success : function(data) {
            fillRecipes(data);
         }
    });
    if(r_filter == 'allrecipes')
    {
        $(".a-tab-nav").attr('class', 'a-tab-nav');
        $("#a_all").attr('class', 'a-tab-nav active');
    }
    else if(r_filter == 'originalrecipes')
    {
        $(".a-tab-nav").attr('class', 'a-tab-nav');
        $("#a_org").attr('class', 'a-tab-nav active');
    }
    else if(r_filter == 'tomatorecipes')
    {
        $(".a-tab-nav").attr('class', 'a-tab-nav');
        $("#a_tomato").attr('class', 'a-tab-nav active');
    }
}


function ajaxInitRecipe(r_filter, r_page)
{
    getRecipeCount();
    preload = $("#preload").data('preload');
    for(i = 1; i <= 8; i++)
    {
        $("#recipe" + i).html('');
    }
    $("#recipe" + 1).html('<img src="' + preload + '" class="true-center preloader">');
    var r_category = $("#filter_select").val();
    $.ajax({
         type: "POST",
         url: "ajax/recipe",
         dataType: "json",
         data: { filter: r_filter, page: r_page, category: r_category },
         success : function(data) {
            fillRecipes(data);
         }
    });
    if(r_filter == 'allrecipes')
    {
        $(".a-tab-nav").attr('class', 'a-tab-nav');
        $("#a_all").attr('class', 'a-tab-nav active');
    }
    else if(r_filter == 'originalrecipes')
    {
        $(".a-tab-nav").attr('class', 'a-tab-nav');
        $("#a_org").attr('class', 'a-tab-nav active');
    }
    else if(r_filter == 'tomatorecipes')
    {
        $(".a-tab-nav").attr('class', 'a-tab-nav');
        $("#a_tomato").attr('class', 'a-tab-nav active');
    }
}


function fillRecipes(data) {

    for(i = 1; i <= 8; i++)
    {
        $("#recipe" + i).html('');
    }
    var recipe_index = 1;
    $.each(data, function(i, recipe)
    {
        var newHTML = '<div hidden>' + recipe['id'] + "</div>";
        //The conditions for the badges
        if(recipe['badge1'] == 1)
        {
            newHTML += '   <img class="mark-kids" src="website/assets/img/mark-kids.png">';
        }
        if(recipe['badge2'] == 1)
        {
            newHTML += '   <img class="mark-bossing" src="website/assets/img/mark-bossing.png">';
        }
        if(recipe['badge3'] == 1)
        {
            newHTML += '   <img class="mark-chef" src="website/assets/img/mark-chef.png">';
        }

        newHTML = newHTML + '   <div class="img-entry lazyload" style="background-image:url('+recipe['photo']+');">';

        //The conditions for the product
        if(recipe['product_id'] == 1)
        {
            newHTML += '        <div class="info cat-original-recipe">';
        }
        else if(recipe['product_id'] == 2)
        {
            newHTML += '        <div class="info cat-tomato-recipe">';
        }
		
		if (recipe['recipe_name'].length > 30) {
			recipe['recipe_name'] = recipe['recipe_name'].substring(0, 30) + '...';
		}
		
        newHTML += '            <h2>' + recipe['recipe_name'] + '</h2>';
        newHTML += '            <p>' + recipe['dish_by'] + '</p>';
        newHTML += '        </div>';
        newHTML += '    </div>';
        $("#recipe" + recipe_index).html(newHTML);
        recipe_index++;
    });
}


function ajaxRate(r_recipe, r_rating)
{

    $.ajax({
         type: "POST",
         url: "ajax/reciperate",
         dataType: "json",
         data: { recipe: r_recipe, rating: r_rating },
         success : function(data) {
            if(data['msg'] == 'S')
            {
                alert('You have successfully rated this recipe!');
                $('.ratings').data('ratings', data['average']), ratings();
            }
            else if(data['msg'] == 'ERR1')
            {
                alert('You have already rated this recipe!');
                $('.ratings').data('ratings', data['average']), ratings();
            }
			$('#featured-total-votes').html(data['total']);
         },
         complete: function(){
            //Add action here for when the ajax process is finished
         }
    });
}