<?php
// Net for all 404s
App::missing(function($exception)
{
    Log::info('User hit a 404. IP: '.Request::server('REMOTE_ADDR'));
    return Response::view('cms.errors.missing', array(), 404);
});

App::error(function($exception, $code)
{
    Log::error($exception);

    if (!Config::get('app.debug')) {
        //return Response::view('errors.index', $code);
        return 'Sorry! Something is wrong with this page! Either the developers are working on this, or you need to log out and log in again.';
    }
});
