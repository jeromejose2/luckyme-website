<?php

/*
|--------------------------------------------------------------------------
| Website Routes
|--------------------------------------------------------------------------
|
| Routes that will be used on the Website Front-end
| part of the website
|
*/

Route::get('login', function()
{
    // just a shortcut to redirecto /login into /manage/login
    return Redirect::route('manage.login');
});

/*
|--------------------------------------------------------------------------
| CMS Routes
|--------------------------------------------------------------------------
|
| Routes that will be used on the CMS part of the website,
| most routes used by the CMS will have a prefix of /manage
|
*/

Route::group(array('prefix' => 'manage'), function()
{
    Route::get('/', function()
    {
        if (Auth::guest())
        {
            return Redirect::route('manage.login');
        }
        else
        {
            return Redirect::route('manage.home');
        }
    });

    Route::get('login', array(
        'as'   => 'manage.login',
        'uses' => 'CMSController@login'
    ));

    Route::post('login', array(
        'as'   => 'manage.postLogin',
        'uses' => 'CMSController@userLogin'
    ));

    Route::get('logout', array(
        'as'   => 'manage.logout',
        'uses' => 'CMSController@userLogout'
    ));

    Route::group(array('before' => 'auth'), function()
    {
        Route::get('home', array(
            'as'   => 'manage.home',
            'uses' => 'CMSController@home'
        ));

        Route::get('profile', array(
            'as'   => 'manage.profile',
            'uses' => 'UsersController@showProfile'
        ));

        Route::get('profile/edit', array(
            'as'   => 'manage.profile-edit',
            'uses' => 'UsersController@editProfile'
        ));

        // Filter first if user is Visitor
        Route::when('manage/settings/create', 'ifVisitor');
        Route::when('manage/settings/*/edit', 'ifVisitor');
        Route::when('manage/dear-bossing/create', 'ifVisitor');
        Route::when('manage/dear-bossing/*/edit', 'ifVisitor');
        Route::when('manage/homepage-sliders/create', 'ifVisitor');
        Route::when('manage/homepage-sliders/*/edit', 'ifVisitor');
        Route::when('manage/badges/create', 'ifVisitor');
        Route::when('manage/badges/*/edit', 'ifVisitor');
        Route::when('manage/ingredients/create', 'ifVisitor');
        Route::when('manage/ingredients/*/edit', 'ifVisitor');
        Route::when('manage/procedures/create', 'ifVisitor');
        Route::when('manage/procedures/*/edit', 'ifVisitor');
        Route::when('manage/ratings/create', 'ifVisitor');
        Route::when('manage/ratings/*/edit', 'ifVisitor');
        Route::when('manage/products/create', 'ifVisitor');
        Route::when('manage/products/*/edit', 'ifVisitor');
        Route::when('manage/recipes/create', 'ifVisitor');
        Route::when('manage/recipes/*/edit', 'ifVisitor');

        Route::group(array('before' => 'isSuperAdmin'), function()
        {
            Route::resource('users', 'UsersController');
        });

        Route::resource('settings', 'SettingsController');
        Route::resource('dear-bossing', 'DearBossingController');
        Route::resource('homepage-sliders', 'HomepageSliderController');
        Route::resource('badges', 'BadgesController');
        Route::resource('ingredients', 'IngredientsController');
        Route::resource('procedures', 'ProceduresController');
        Route::resource('ratings', 'RatingsController');
        Route::resource('products', 'ProductsController');
        Route::resource('recipes', 'RecipesController');
        Route::resource('categories', 'CategoriesController');

        Route::group(array('prefix' => 'ajax'), function()
        {
            Route::get('users', array(
                'as'   => 'ajax.users',
                'uses' => 'UsersController@getDatatable'));
            Route::get('dear-bossing', array(
                'as'   => 'ajax.dear-bossing',
                'uses' => 'DearBossingController@getDatatable'));
            Route::get('settings', array(
                'as'   => 'ajax.settings',
                'uses' => 'SettingsController@getDatatable'));
            Route::get('homepage-slider', array(
                'as'   => 'ajax.homepage-slider',
                'uses' => 'HomepageSliderController@getDatatable'));
            Route::get('product', array(
                'as'   => 'ajax.product',
                'uses' => 'ProductsController@getDatatable'));
            Route::get('recipe', array(
                'as'   => 'ajax.recipe',
                'uses' => 'RecipesController@getDatatable'));
            Route::get('category', array(
                'as'   => 'ajax.category',
                'uses' => 'CategoriesController@getDatatable'));
        });
    });
});

