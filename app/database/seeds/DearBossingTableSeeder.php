<?php

use Faker\Factory as Faker;

class DearBossingTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker::create();

        foreach(range(1, 10) as $index)
        {
            DB::table('dear_bossing')->insert(array(
                'episode_id'    => $index,
                'title'         => $faker->word,
                'article'       => $faker->realText($maxNbChars = 200, $indexSize = 2),
                'youtube_url'   => $faker->url,
                'is_active'     => $faker->boolean($chanceOfGettingTrue = 50),
                'email_address' => $faker->email
                )
            );
        }
    }

}