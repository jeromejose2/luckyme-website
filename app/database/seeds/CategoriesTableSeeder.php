<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CategoriesTableSeeder extends Seeder {

	public function run()
	{
		Category::create([
            'name' => 'All'
        ]);
        Category::create([
            'name' => 'None'
        ]);
	}

}