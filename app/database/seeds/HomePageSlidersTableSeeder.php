<?php

use Faker\Factory as Faker;

class HomePageSlidersTableSeeder extends Seeder {

    public function run()
    {
        //Insert normal slider
        DB::table('homepage_sliders')->insert(array(
            'slider_id'             => 1,
            'photo'                 => 'website/assets/img/slider-img1.jpg',
            'button1_url_copyline1' => 'Try our Original NamNam Recipes.',
            'button1_url_copyline2' => 'Click here.',
            'button1_fontcolor'     => '',
            'button1_bgcolor'       => '',
            'button1_url_bg'        => URL::to('/#recipe'),
            'button2_url_copyline1' => 'Try our Tomato NamNam Recipes.',
            'button2_url_copyline2' => 'Click here.',
            'button2_fontcolor'     => '',
            'button2_bgcolor'       => '',
            'button2_url_bg'        => URL::to('/#recipe'),
            'bg_video_url'          => '',
            'content_type'          => 'Image'
            )
        );

        //Insert video slider
        DB::table('homepage_sliders')->insert(array(
            'slider_id'             => 2,
            'photo'                 => '',
            'button1_url_copyline1' => '',
            'button1_url_copyline2' => '',
            'button1_fontcolor'     => '',
            'button1_bgcolor'       => '',
            'button1_url_bg'        => '',
            'button2_url_copyline1' => '',
            'button2_url_copyline2' => '',
            'button2_fontcolor'     => '',
            'button2_bgcolor'       => '',
            'button2_url_bg'        => '',
            'bg_video_url'          => 'jFg_8u87zT0',
            'content_type'          => 'Video'
            )
        );

        //Insert normal slider with video url. Video takes priority
        DB::table('homepage_sliders')->insert(array(
            'slider_id'             => 1,
            'photo'                 => 'website/assets/img/slider-img1.jpg',
            'button1_url_copyline1' => 'Try our Original NamNam Recipes.',
            'button1_url_copyline2' => 'Click here.',
            'button1_bgcolor'       => '',
            'button1_url_bg'        => '',
            'button1_url_bg'        => URL::to('/#recipe'),
            'button2_url_copyline1' => 'Try our Tomato NamNam Recipes.',
            'button2_url_copyline2' => 'Click here.',
            'button2_fontcolor'     => '',
            'button2_bgcolor'       => '',
            'button2_url_bg'        => URL::to('/#recipe'),
            'bg_video_url'          => 'jFg_8u87zT0',
            'content_type'          => 'Image'
            )
        );
    }

}