<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class RecipesTableSeeder extends Seeder {

	public function run()
	{
		Recipe::create([
            'product_id' => 1,
            'recipe_name' => 'NamNam',
            'prep_time'  => 1,
            'dish_by'    => 'Me',
            'serving_size' => '1',
            'is_featured' => 0,
            'rating_count' => 0,
            'photo'      => 'website/assets/img/featured-preview.png',
            'description' => 'This is a dummy recipe for the namnam website.',
            'category'    => 'None'
		]);
	}

}