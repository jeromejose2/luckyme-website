<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder {

	public function run()
	{
			DB::table('products')->insert(array(
                'title'    => 'NamNam Original',
                'description'         => 'NamNam with taste of real meat, bones and spices.',
                'namnam_product_shot'   => 'website/assets/img/company-logo.png',
                'url'     => 'www.google.com'
                )
            );

            DB::table('products')->insert(array(
                'title'    => 'NamNam Tomato',
                'description'         => 'NamNam with taste of real tomato.',
                'namnam_product_shot'   => 'website/assets/img/pack-namnam.png',
                'url'     => 'www.google.com'
                )
            );
	}

}