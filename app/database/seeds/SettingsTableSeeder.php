<?php

use Faker\Factory as Faker;

class SettingsTableSeeder extends Seeder {

    public function run()
    {
        // $faker = Faker::create();

        // foreach(range(1, 10) as $index)
        // {
        //     DB::table('settings')->insert(array(
        //         'content' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        //         'type'    => $faker->randomElement($array = array("chef babes' corner", "manilyn's tipid tips"))
        //         )
        //     );
        // }

        DB::table('settings')->insert(array(
                'content' => 'NamNam Website',
                'type'    => 'site_title'
                )
            );


        DB::table('settings')->insert(array(
                'content' => 'All you need is love and NamNam!',
                'type'    => 'product_header'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'webnamnam@gmail.com',
                'type'    => 'to_email'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'NamNam Site Admin',
                'type'    => 'to_name'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'website/assets/music/bgm.mp3',
                'type'    => 'site_bgm'
            ));

        DB::table('settings')->insert(array(
                'content' => 'website/assets/img/product-namnam-ingre.png',
                'type'    => 'product_header_img'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'Chef Babes Corner',
                'type'    => 'chef_btn'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'Former Malacañang Palace head chef Babes Austria <br>  answers most frequenty asked questions.',
                'type'    => 'chef_btn_desc'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'Chef Babes\' Corner',
                'type'    => 'chef_title'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'This is a test content! Imagine this has four paragraphs of text!',
                'type'    => 'chef_content'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'Manilyn\'s Tipid Tips' ,
                'type'    => 'artist_btn'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'Actress and mom Manilyn Reynes shares <br> kitchen tipid tips.',
                'type'    => 'artist_btn_desc'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'Manilyn Reynes\' Tipid Tips',
                'type'    => 'artist_title'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'This is a test content! Imagine this has four paragraphs of text!',
                'type'    => 'artist_content'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'NamNam Cooking Videos',
                'type'    => 'video_btn_top'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'Watch and learn how to cook with NamNam!',
                'type'    => 'video_btn_bottom'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'https://www.youtube.com/watch?v=NNv4v5Io-50&amp;list=PLGRhcC_vtOrZuV5vu6XzXDWsja3wtfDfN',
                'type'    => 'video_btn_url'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'MONDE NISSIN CORPORATION',
                'type'    => 'corporation'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'Phone: (02) 810-3550 | 815-6676 | (02) 812-3392 | 810-9207',
                'type'    => 'phone'
                )
            );

        DB::table('settings')->insert(array(
                'content' => '22 & 23 Floor, 6750 Office Tower, Ayala Avenue, Makati City',
                'type'    => 'address'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'Copyright 2014, Monde Nissin Corporation. All rights reserved.',
                'type'    => 'copyright'
                )
            );
        DB::table('settings')->insert(array(
                'content' => 'http://www.youtube.com',
                'type'    => 'social_youtube'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'http://www.facebook.com',
                'type'    => 'social_facebook'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'http://www.twitter.com',
                'type'    => 'social_twitter'
                )
            );

        DB::table('settings')->insert(array(
                'content' => 'namnamPH',
                'type'    => 'social_twitter_via'
                )
            );

    }

}