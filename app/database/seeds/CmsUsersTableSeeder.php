<?php

class CmsUsersTableSeeder extends Seeder {

    public function run()
    {
        User::create([
            'username' => 'admin',
            'password' => 'pass123',
            'access_level' => 'super admin',
            'name' => 'Administrator',
            'email' => 'admin@admin.com'
        ]);

        User::create([
            'username' => 'john',
            'password' => 'pass123',
            'access_level' => 'admin',
            'name' => 'John Doe',
            'email' => 'superadmin@admin.com'
        ]);
    }

}