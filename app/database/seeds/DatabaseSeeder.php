<?php

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $this->call('CmsUsersTableSeeder');
        $this->call('DearBossingTableSeeder');
        $this->call('SettingsTableSeeder');
        $this->call('HomePageSlidersTableSeeder');
        $this->call('ProductsTableSeeder');
        $this->call('CategoriesTableSeeder');
        $this->call('RecipesTableSeeder');
    }

}
