<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHomepageSlidersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homepage_sliders', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('slider_id');
            $table->string('photo');
            $table->string('button1_url_copyline1');
            $table->string('button1_url_copyline2');
            $table->string('button1_fontcolor');
            $table->string('button1_bgcolor');
            $table->string('button1_url_bg');
            $table->string('button2_url_copyline1');
            $table->string('button2_url_copyline2');
            $table->string('button2_fontcolor');
            $table->string('button2_bgcolor');
            $table->string('button2_url_bg');
            $table->string('bg_video_url');
            $table->string('content_type');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('homepage_sliders');
    }

}
