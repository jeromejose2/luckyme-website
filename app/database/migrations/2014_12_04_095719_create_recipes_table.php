<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecipesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recipes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('product_id');
			$table->string('recipe_name');
            $table->integer('badge1');
            $table->integer('badge2');
            $table->integer('badge3');
			$table->string('prep_time');
			$table->string('dish_by');
			$table->string('serving_size');
			$table->string('is_featured');
			$table->string('rating_count');
			$table->string('photo');
            $table->string('description');
            $table->string('category');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recipes');
	}

}
