<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDearBossingTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dear_bossing', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('episode_id');
            $table->string('title');
            $table->longText('article');
            $table->string('youtube_url');
            $table->boolean('is_active');
            $table->string('email_address');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dear_bossing');
    }

}
