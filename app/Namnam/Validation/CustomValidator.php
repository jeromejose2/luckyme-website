<?php namespace Namnam\Validation;

use Illuminate\Validation\Validator;

class CustomValidator extends Validator {

    public function validateAlphaSpaces($attribute, $value, $params)
    {
        return preg_match('/^[\pL\s]+$/u', $value);
    }
}