<?php

/*
|--------------------------------------------------------------------------
| Website Routes
|--------------------------------------------------------------------------
|
| Routes that will be used on the Website Front-end
| part of the website
|
*/

Route::group([],function()
{

    Route::get('/', function()
    {
        //Display home page.
        if(Input::has('recipe_id'))
        {
            $recipe = Input::get('recipe_id');
            $recipe = Recipe::find($recipe);
            if(empty($recipe))
            {
                $recipe = 'none';
            }
        }
        else
        {
            $recipe = 'none';
        }
        $featured = Recipe::where('is_featured', 1)->orderBy('updated_at', 'desc')->first();
        $ingredients = 'none';
        $procedures = 'none';
        if(empty($featured))
        {
            $featured = 'none';
            $average  = 0;
			$totalVotes = 0;
            $featured_product = 'none';
        }
        else
        {
            $average = Rating::getAverage($featured->id);
			$totalVotes = Rating::getTotal($featured->id);
            $featured_product = Product::where('id', $featured->product_id)->first();
            $ingredients = Ingredient::where('recipe_id', $featured->id)->get();
            $procedures = Procedure::where('recipe_id', $featured->id)->get();

        }
        $sliders = HomepageSlider::where('slider_id', '!=', '0')->orderBy('slider_id')->get();
        $products = Product::all();
        if(empty($products))
        {
            $products = 'none'; // Assign none if Products model is empty
        }

        $settings = Setting::all();
        if(empty($settings))
        {
            $settings = 'none';
        }

        //Loop through settings to assign value to an array for more compact frontend code
        $sets = [];
        foreach($settings as $s)
        {
            $sets[$s->type] = $s->content ;
        }

        $categories = Category::lists('name', 'name');

        return View::make('website.index2')
            ->with('categories', $categories)
            ->with('recipe', $recipe)
            ->with('ingredients', $ingredients)
            ->with('procedures', $procedures)
            ->with('featured', $featured)
            ->with('sliders', $sliders)
            ->with('products', $products)
            ->with('settings', $sets)
            ->with('average', $average)
			->with('total_votes', $totalVotes)
            ->with('featured_product', $featured_product);
    });

    Route::get('inner-recipe-popup.php', function()
    {
        $data = Input::all();
        $result = Recipe::where('id', $data['id'])->first();
        $product = Product::where('id', $result->product_id)->first();
        $rating = Rating::getAverage($data['id']);
		$totalVotes = Rating::getTotal($data['id']);

        $ingredients = Ingredient::where('recipe_id', $result->id)->get();
        $procedures = Procedure::where('recipe_id', $result->id)->get();

        $via = Setting::where('type', 'social_twitter_via')->first();

        return View::make('website.inner-recipe-popup')
                ->with('via', $via->content)
                ->with('ingredients', $ingredients)
                ->with('procedures', $procedures)
                ->with('data', $result)
                ->with('product', $product)
                ->with('average', $rating)
				->with('total_votes', $totalVotes);
    });

    // Route::get('recipe/{id}', function($id)
    // {
    //     $data = $id;
    //     $result = Recipe::where('id', $data)->first();
    //     if(empty($result))
    //     {
    //         return App::abort('404');
    //     }
    //     $site_title = Setting::where('type', 'site_title')->first()->content;

    //     if(empty($result))
    //     {
    //         $result = 'none';
    //         $average  = 0;
    //         $featured_product = 'none';
    //     }
    //     else
    //     {
    //         $average = Rating::getAverage($result->id);
    //         $featured_product = Product::where('id', $result->product_id)->first();
    //         $ingredients = Ingredient::where('recipe_id', $result->id)->get();
    //         $procedures = Procedure::where('recipe_id', $result->id)->get();

    //     }
    //     $via = Setting::where('type', 'social_twitter')->first();
    //     $bgm = Setting::where('type', 'site_bgm')->first();
    //     return View::make('website.recipe')
    //             ->with('bgm', $bgm)
    //             ->with('via', $via->content)
    //             ->with('average', $average)
    //             ->with('site_title', $site_title)
    //             ->with('ingredients', $ingredients)
    //             ->with('procedures', $procedures)
    //             ->with('data', $result)
    //             ->with('product', $featured_product);
    // });

    Route::get('chefbabespopup.php', function()
    {
        $title = Setting::where('type', 'chef_title')->first();
        $content = Setting::where('type', 'chef_content')->first();
        if(empty($title))
        {
            $title = 'none';
        }
        if(empty($content))
        {
            $content = 'none';
        }
        return View::make('website.chefbabespopup')
                    ->with('title', $title)
                    ->with('content', $content);
    });

    Route::get('artistmpopup.php', function()
    {
        $title = Setting::where('type', 'artist_title')->first();
        $content = Setting::where('type', 'artist_content')->first();
        if(empty($title))
        {
            $title = 'none';
        }
        if(empty($content))
        {
            $content = 'none';
        }
        return View::make('website.artistmpopup')
                    ->with('title', $title)
                    ->with('content', $content);
    });

    Route::post('mail', array('as' => 'website.mail', function() {
         // don't convert the tabs into spaces
        $data = Input::all();
        Mail::send('cms.emails.feedback', array('data' => $data), function($message)
        {
            $data = Input::all();
            $to_email = Setting::where('type', 'to_email')->first()->content;
            $to_name = Setting::where('type', 'to_name')->first()->content;
            //name email contact address subject message
            $message->from($data['email'], $data['name']);
            $message->to($to_email, $to_name)->subject('Customer feedback from NamNam Website (Subject:'.$data['subject'].')');
        });
        return Redirect::to('/');
    }));

    Route::group(array('prefix' => 'ajax'), function()
    {
        Route::post('recipe', array('as' => 'ajax.recipe', function()
        {
            if(Request::ajax())
            {
                $data = Input::all();
                $offset = 8 * ($data['page']-1);

                if($data['filter'] == 'allrecipes')
                {
                    $filter = 'all';
                    if($data['category'] == 'All')
                    {
                        $db = Recipe::limit(8)->offset($offset)->get();
                    }
                    else
                    {
                        $db = Recipe::where('category', $data['category'])->limit(8)->offset($offset)->get();
                    }

                }
                elseif($data['filter'] == 'tomatorecipes')
                {
                    $filter = 'tomato';
                    if($data['category'] == 'All')
                    {
                        $db = Recipe::where('product_id', 2)->limit(8)->offset($offset)->get();
                    }
                    else
                    {
                        $db = Recipe::where('product_id', 2)->where('category', $data['category'])->limit(8)->offset($offset)->get();
                    }
                }
                elseif($data['filter'] == 'originalrecipes')
                {
                    $filter = 'original';
                    if($data['category'] == 'All')
                    {
                        $db = Recipe::where('product_id', 1)->limit(8)->offset($offset)->get();
                    }
                    else
                    {
                        $db = Recipe::where('product_id', 1)->where('category', $data['category'])->limit(8)->offset($offset)->get();
                    }
                }

                return Response::json($db);
            }
        }));

        Route::post('recipecount', array('as' => 'ajax.recipecount', function()
        {
            if(Request::ajax())
            {
                $data = Input::all();
                if($data['category'] == 'All')
                {
                    $r = [];
                    $r['all'] = Recipe::count();
                    $r['org'] = Recipe::where('product_id', 1)->count();
                    $r['tom'] = Recipe::where('product_id', 2)->count();
                }
                else
                {
                    $r = [];
                    $r['all'] = Recipe::where('category', $data['category'])->count();
                    $r['org'] = Recipe::where('category', $data['category'])->where('product_id', 1)->count();
                    $r['tom'] = Recipe::where('category', $data['category'])->where('product_id', 2)->count();

                }
                return Response::json($r);
            }
        }));

        Route::post('reciperate', array('as' => 'ajax.reciperate', function()
        {
            if(Request::ajax())
            {
                $data = Input::all();
                $data = Input::all();
                $ip   = Request::getClientIp();
                $rating = $data['rating'];
                $recipe_id = $data['recipe'];
                $response = [];
                //check if ip exists
                if(Rating::addRating($recipe_id, $rating, $ip))
                {
                    //Start computing ratings and return new rating
                    $response['msg'] = 'S';
                }
                else
                {
                    $response['msg'] = 'ERR1';
                }
                $response['average'] = Rating::getAverage($recipe_id);
				$response['total'] = Rating::getTotal($recipe_id);
                return Response::json($response);
            }
        }));
    });
    
    Route::group(array('prefix' => 'recipe/print'), function()
    {
    	Route::get('preview/{id}', function ($id) {
    		$result = Recipe::where('id', $id)->first();
    		 
    		if(empty($result))
    		{
    			return App::abort('404');
    		}
    		else
    		{
    			$ingredients = Ingredient::where('recipe_id', $result->id)->get();
    			$procedures = Procedure::where('recipe_id', $result->id)->get();
    		}
    		 
    		return View::make('website.printable.index')
    		->with('ingredients', $ingredients)
    		->with('procedures', $procedures)
    		->with('recipe', $result);
    	});
    	
    	Route::get('{id}', function ($id) {
    		$data['recipe'] = Recipe::where('id', $id)->first();
    			 
    		if(empty($data['recipe']))
    		{
    			return App::abort('404');
    		}
    		else
    		{
    			$data['ingredients'] = Ingredient::where('recipe_id', $data['recipe']->id)->get();
    			$data['procedures'] = Procedure::where('recipe_id', $data['recipe']->id)->get();
    		}
    		
    		$pdf = PDF::loadView('website.printable.pdf', $data);

    		return $pdf->download(str_replace(' ', '_', $data['recipe']->recipe_name).'_Recipe.pdf');
    	});
    });
});
