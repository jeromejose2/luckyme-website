@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Create User</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create User</h1>
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    {{ Form::open(array('route' => 'manage.ratings.store')) }}
                        <div class="form-group">
                        1{{Form::radio('rating', '1', true, array('class' => 'name'))}}<br>
                        2{{Form::radio('rating', '2')}}<br>
                        3{{Form::radio('rating', '3')}}<br>
                        4{{Form::radio('rating', '4')}}<br>
                        5{{Form::radio('rating', '5')}}
                        </div>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary btn-block')) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop