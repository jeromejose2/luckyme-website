@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Create Recipe</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create Recipe</h1>
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    {{ Form::open(array('route' => 'manage.recipes.store','files' => true)) }}
                        <div class="form-group">
                            {{ Form::label('product', 'Select Product', array('class' => 'label-control')) }}
                            {{ Form::select('product', $products, 0, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('recipe', 'Recipe Name') }}
                            {{ Form::text('recipe', null, array('class' => 'form-control', 'placeholder' => 'Recipe Name')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('category', 'Category', array('class' => 'label-control'))}}
                            {{ Form::select('category', $categories, null, array('class' => 'form-control'))}}
                        </div>
                        <div class="form-group">
                            {{ Form::label('prep_time', 'Preparation Time') }}
                            {{ Form::text('prep_time', null, array('class' => 'form-control', 'placeholder' => 'Preparation Time')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('dish', 'Dish By') }}
                            {{ Form::text('dish', null, array('class' => 'form-control', 'placeholder' => 'Dish By')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('serving', 'Serving Size') }}
                            {{ Form::text('serving', null, array('class' => 'form-control', 'placeholder' => 'Serving Size')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::checkbox('featured', '1', true)}} Featured
                        </div>
                        <div class="form-group">
                             {{ Form::checkbox('badge1', 1, true)}} Kids' Favorite
                        </div>
                        <div class="form-group">
                             {{ Form::checkbox('badge2', 1, true)}} Bossing-approved
                        </div>
                        <div class="form-group">
                             {{ Form::checkbox('badge3', 1, true)}} Chef Babes-approved
                        </div>

                        <div class="form-group">
                            {{ Form::textarea('description', null, array('class' => 'form-control', 'placeholder' => 'Description for recipe'))}}
                        </div>

                        <div class="form-group">
                        <div class="col-sm-9">
                            {{ Form::file('Image-Photos') }}
                            <p class="help-block">Photos for Image, Only .jpg, .png, .gif, .bmp allowed.</p>
                        </div>
                        </div>

                        {{ Form::submit('Save', array('class' => 'btn btn-primary btn-block')) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->
@stop

@section('page-specific-js')
<script type="text/javascript">
	$( "textarea" ).jqte();
</script>
@stop