@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Edit Recipe</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Recipe</h1>
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    {{ Form::open(array('route' => array('manage.recipes.update',$recipe->id),'method' => 'put','files' => true)) }}
                        <div class="form-group">
                            {{ Form::submit('Edit Ingredients and Procedures', array('name' => 'edit_ing_pro', 'class' => 'btn btn-warning'))}}
                        </div>

                        <div class="form-group">
                            {{ Form::label('product', 'Select Product', array('class' => 'label-control')) }}
                            {{ Form::select('product_id', $products, $recipe->product_id, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('recipe', 'Recipe Name') }}
                            {{ Form::text('recipe_name', $recipe->recipe_name, array('class' => 'form-control', 'placeholder' => 'Recipe Name')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('category', 'Category', array('class' => 'label-control'))}}
                            {{ Form::select('category', $categories, $recipe->category, array('class' => 'form-control'))}}
                        </div>
                        <div class="form-group">
                            {{ Form::label('prep_time', 'Preparation Time') }}
                            {{ Form::text('prep_time', $recipe->prep_time, array('class' => 'form-control', 'placeholder' => 'Preparation Time')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('dish', 'Dish By') }}
                            {{ Form::text('dish_by', $recipe->dish_by, array('class' => 'form-control', 'placeholder' => 'Dish By')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('serving', 'Serving Size') }}
                            {{ Form::text('serving_size', $recipe->serving_size, array('class' => 'form-control', 'placeholder' => 'Serving Size')) }}
                        </div>
                        <div class="form-group">
                        @if($recipe->is_featured==1)
                            {{ Form::checkbox('is_featured', '1', true)}} Featured
                        @else
                            {{ Form::checkbox('is_featured', '1', false)}} Featured
                        @endif
                        </div>
                        <div class="form-group">
                            @if($recipe->badge1==1)
                             {{ Form::checkbox('badge1', 1,true)}} Kids' Favorite
                            @else
                            {{ Form::checkbox('badge1', 1, false)}} Kids' Favorite
                            @endif
                        </div>
                        <div class="form-group">
                            @if($recipe->badge2==1)
                             {{ Form::checkbox('badge2', 1,true)}} Bossing - approved
                            @else
                            {{ Form::checkbox('badge2', 1, false)}} Bossing - approved
                            @endif
                        </div>
                        <div class="form-group">
                            @if($recipe->badge3==1)
                             {{ Form::checkbox('badge3', 1, true)}} Chef Babes-approved
                            @else
                            {{ Form::checkbox('badge3', 1, false)}} Chef Babes-approved
                            @endif
                        </div>

                        <div class="form-group">
                            {{ Form::textarea('description', $recipe->description, array('class' => 'form-control', 'placeholder' => 'Description for recipe'))}}
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9">
                                {{ HTML::image($recipe->photo, $alt="namnam") }}
                                {{ Form::file('Image-Photos') }}
                                <p class="help-block">Photos for Image, Only .jpg, .png, .gif, .bmp allowed.</p>
                            </div>
                        </div>

                        {{ Form::submit('Save', array('class' => 'btn btn-primary btn-block')) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop

@section('page-specific-js')
<script type="text/javascript">
	$( "textarea" ).jqte();
</script>
@stop