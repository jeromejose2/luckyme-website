@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Recipe View All</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Recipe <a href="{{ URL::route('manage.recipes.create') }}" class="btn btn-default"> Add New</a></h1>
        </div><!-- /.col-lg-12 -->
		 <div class="col-lg-12">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>RecipeName</th>
                        <th>PrepTime</th>
                        <th>dish_by</th>
                        <th>serving_size</th>


                    </tr>
                </thead>
                <tbody>
                	<div class="col-lg-12">
					<td>{{ $recipe['id'] }}</td>
                    <td>{{ $recipe['recipe_name'] }}</td>
                    <td>{{ $recipe['prep_time'] }}</td>
                    <td>{{ $recipe['dish_by'] }}</td>
                    <td>{{ $recipe['serving_size'] }}</td>
					</div>
                </tbody>
            </table>
        </div>
        <h2>Procedures</h2>
        <div class="col-lg-12">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Name</th>
                        
                    </tr>
                </thead>
                <tbody>
					@foreach ($procedures as $procedure)
					<tr>
                    <td>{{$procedure->name}}</td>
					<td>{{$procedure->item}}</td>
					
					</tr>
					@endforeach
                </tbody>
            </table>
        </div>

        <h2>Ingredients</h2>
        <div class="col-lg-12">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Qty</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($ingredients as $ingredient)
                    <tr>
                    <td>{{$ingredient->name}}</td>
                    <td>{{$ingredient->qty}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop