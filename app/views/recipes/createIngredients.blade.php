@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Create Recipe</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add Ingredients and Procedures</h1>
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    {{ Form::open(array('route' => 'manage.recipes.store')) }}
                        {{ Form::hidden('ingredients_mode', 'true')}}
                        <div class="form-group">
                            {{ Form::label('recipe_id_label', 'Adding ingredients to : '.$recipe->id.' - '.$recipe->recipe_name, array('class' => 'label-control')) }}
                            {{ Form::hidden('recipe_id', $recipe->id)}}
                        </div>

                        <div class="form-group">
                            {{ Form::label('name_label', 'Ingredient Name') }}
                            {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Ingredient Name')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('qty_label', 'Quantity') }}
                            {{ Form::text('qty', null, array('class' => 'form-control', 'placeholder' => 'Quantity')) }}
                        </div>

                        {{ Form::submit('Add', array('class' => 'btn btn-primary btn-block')) }}
                    {{ Form::close() }}
                </div>
                <div class="col-md-4 col-md-offset-1">
                    {{ Form::open(array('route' => 'manage.recipes.store')) }}
                        {{ Form::hidden('procedures_mode', 'true')}}
                        <div class="form-group">
                            {{ Form::label('recipe_id_label', 'Adding procedures to : '.$recipe->id.' - '.$recipe->recipe_name, array('class' => 'label-control')) }}
                            {{ Form::hidden('recipe_id', $recipe->id)}}
                        </div>

                        <div class="form-group">
                            {{ Form::label('name_label', 'Procedure Name') }}
                            {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Procedure Name')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('item_label', 'Procedure') }}
                            {{ Form::textarea('item', null, array('class' => 'form-control', 'placeholder' => 'Procedure')) }}
                        </div>

                        {{ Form::submit('Add', array('class' => 'btn btn-primary btn-block')) }}
                    {{ Form::close() }}
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    Ingredients: <br>
                    <ul>
                    @foreach($ingredients as $i)
                    <!-- Button trigger modal -->
                    <li>

                        {{ Form::open(array('route' => 'manage.recipes.store', 'class' => 'form'))}}
                        {{ Form::hidden('ingredients_mode', 'true')}}
                        {{ Form::submit('Edit', array('name' => 'edit_btn') )}}
                        {{ Form::hidden('delete', 'true')}}
                        {{ Form::hidden('recipe_id', $recipe->id)}}
                        {{ Form::hidden('id', $i->id)}}
                        <button type="button" data-toggle="modal" data-target="#ing{{$i->id}}">
                          Remove
                        </button>
                        {{ Form::close()}}

                        {{ $i->qty }} {{ $i->name }}

                    </li>
                    <!-- Modal -->
                    <div class="modal fade" id="ing{{$i->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Remove Confirmation</h4>
                          </div>
                          <div class="modal-body">
                            Are you sure you want to remove?

                          </div>
                          <div class="modal-footer">
                            {{ Form::open(array('route' => 'manage.recipes.store'))}}
                            {{ Form::hidden('ingredients_mode', 'true')}}
                            {{ Form::submit('Remove', array('name' => 'remove_btn','class' => 'btn btn-danger')) }}
                            {{ Form::hidden('delete', 'true')}}
                            {{ Form::hidden('recipe_id', $recipe->id)}}
                            {{ Form::hidden('id', $i->id)}}
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            {{ Form::close()}}

                          </div>
                        </div>
                      </div>
                    </div>

                    @endforeach
                    </ul>
                </div>
                <div class="col-md-4 col-md-offset-1">
                    Procedures: <br>
                    <ul>
                    @foreach($procedures as $p)
                        <li>
                            {{ Form::open(array('route' => 'manage.recipes.store'))}}
                            {{ Form::hidden('procedures_mode', 'true')}}
                            {{ Form::submit('Edit', array('name' => 'edit_btn') )}}
                            {{ Form::hidden('delete', 'true')}}
                            {{ Form::hidden('recipe_id', $recipe->id)}}
                            {{ Form::hidden('id', $p->id)}}
                            <button type="button" data-toggle="modal" data-target="#pro{{$p->id}}">
                              Remove
                            </button>
                            {{ Form::close()}}
                            {{ $p->name }} <br>
                            {{ nl2br($p->item) }}
                        </li>
                        <div class="modal fade" id="pro{{$p->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Remove Confirmation</h4>
                              </div>
                              <div class="modal-body">
                                Are you sure you want to remove?

                              </div>
                              <div class="modal-footer">
                                {{ Form::open(array('route' => 'manage.recipes.store'))}}
                                {{ Form::hidden('procedures_mode', 'true')}}
                                {{ Form::submit('Remove', array('name' => 'remove_btn', 'class' => 'btn btn-danger')) }}
                                {{ Form::hidden('delete', 'true')}}
                                {{ Form::hidden('recipe_id', $recipe->id)}}
                                {{ Form::hidden('id', $p->id)}}
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                {{ Form::close()}}
                              </div>
                            </div>
                          </div>
                         </div>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop