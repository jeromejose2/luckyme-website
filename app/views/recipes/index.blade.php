@extends('cms.templates.chumper')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Recipe View All</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Recipes <a href="{{ URL::route('manage.recipes.create') }}" class="btn btn-default"> Add New</a></h1>
        </div><!-- /.col-lg-12 -->
        <div class="col-lg-6">
            Total of {{$count}} recipes. {{$original_count}} original and {{$tomato_count}} tomato.
        </div>
        <div class="col-lg-12">
            {{ Datatable::table()
                ->addColumn('Product ID', 'Recipe Name', 'Category', 'Prep. Time', 'Dish By', 'Serving Size', 'Featured', 'Kids\'s Favorite', 'Bossing-pproved', 'Chef Babes-approved', '', '', '', '')
                ->setUrl(route('ajax.recipe'))
                ->setOptions('dom', '<"row"<"col-lg-6 col-md-6"f><"col-lg-6 col-md-6"<"pull-right"l>>><t><ip>')
                ->setId('datatable')
                ->render('cms.templates.sections.datatable') }}
        </div>
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop