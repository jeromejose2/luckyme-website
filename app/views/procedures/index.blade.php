@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Procedure View All</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Procedure Settings <a href="{{ URL::route('manage.procedures.create') }}" class="btn btn-default"> Add New</a></h1>
        </div><!-- /.col-lg-12 -->
        <div class="col-lg-12">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Recipe Name</th>
                        <th>Item</th>
                        <th>Procedure Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
   				 @foreach ($procedures as $procedure)
                        <tr>
                            <td>{{ Recipe::find($procedure->recipe_id)->recipe_name }}</td>
                            <td>{{ $procedure->item }}</td>
                            <td>{{ $procedure->name }}</td>
                            <td>{{ HTML::linkRoute('manage.procedures.edit','Edit', $procedure->id  )}}</td>
                        </tr>
  				  @endforeach
                </tbody>
            </table>
        </div>
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop