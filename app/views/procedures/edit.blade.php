@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Edit Procedure</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Procedure</h1>
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                 {{ Form::open(array('route' => array('manage.procedures.update',$procedure->id),'method' => 'put','files' => true)) }}
                        <div class="form-group">
                            {{-- Form::label('recipe', 'Select Recipe', array('class' => 'label-control')) --}}
                            {{-- Form::select('recipe_id', $recipes, $procedure->recipe_id, array('class' => 'form-control')) --}}
                            {{ Form::hidden('recipe_id', $procedure->recipe_id)}}
                            {{ Form::label('recipe_name', 'Recipe : '.$recipe->recipe_name)}}
                        </div>
                        <div class="form-group">
                            {{ Form::label('name', 'Procedure Name') }}
                            {{ Form::text('name', $procedure->name, array('class' => 'form-control', 'placeholder' => 'Procedure Name')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('item', 'Procedure') }}
                            {{ Form::textarea('item', $procedure->item, array('class' => 'form-control', 'placeholder' => 'Procedure')) }}
                        </div>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary btn-block')) }}
                        @if(isset($recipe_id))
                            {{Form::hidden('recipe_id', $recipe_id)}}
                        @endif
                    {{ Form::close() }}
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop