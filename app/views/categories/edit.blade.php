
@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Edit Category</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Category</h1>
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    {{ Form::open(array('route' => array('manage.categories.update',$category->id),'method' => 'put')) }}
                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', $category->name , array('class' => 'form-control', 'placeholder' => $category->name )) }}
                        </div>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary btn-block')) }}
                    {{ Form::close() }}
                    {{ Form::open(array('route' => array('manage.categories.destroy', $category->id), 'method' => 'DELETE'))}}
                        {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-block'))}}
                    {{ Form::close()}}
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop

