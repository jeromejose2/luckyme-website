@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Create Category</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create Category</h1>
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    {{ Form::open(array('route' => 'manage.categories.store')) }}
                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Category Name')) }}
                        </div>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary btn-block')) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop