@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Create User</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create User</h1>
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    {{ Form::open(array('route' => 'manage.badges.store','files' => true)) }}
                        <div class="form-group">
                            {{ Form::label('product', 'Select Product', array('class' => 'label-control')) }}
                            {{ Form::select('recipe', $recipes, 0, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                        Kid's Favorite{{Form::radio('approval', '1', true, array('class' => 'name'))}}<br>
                        Bossing Approved{{Form::radio('approval', '2')}}<br>
                        Chef Babes Approved{{Form::radio('approval', '3')}}
                        </div>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary btn-block')) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop