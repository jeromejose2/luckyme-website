@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Product View All</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Product Settings <a href="{{ URL::route('manage.products.create') }}" class="btn btn-default"> Add New</a></h1>
        </div><!-- /.col-lg-12 -->
		 <div class="col-lg-12">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Product ID</th>
                        <th>Title</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                	<div class="col-lg-12">
						<td>{{ $product['id'] }}</td>
						<td>{{ $product['title'] }}</td>
						<td>{{ $product['description'] }}</td>
					</div>
                </tbody>
            </table>
        </div>
        <div class="col-lg-12">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Recipe ID</th>
                        <th>Recipe Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
					@foreach ($recipes as $recipe)
					<tr>
					<td>{{$recipe->id}}</td>
					<td>{{$recipe->recipe_name}}</td>
					<td>{{HTML::linkRoute('manage.recipes.show', 'View more', $recipe->id)}}</td>
					</tr>
					@endforeach
                </tbody>
            </table>
        </div>
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop