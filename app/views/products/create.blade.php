@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Create Product</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create Product</h1>
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    {{ Form::open(array('route' => 'manage.products.store','files' => true)) }}
                        <div class="form-group">
                            {{ Form::label('title', 'Title') }}
                            {{ Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Title')) }}
                        </div>

                        <div class="form-group">
                        <div class="col-sm-9">
                            {{ Form::file('Image-NamNam') }}
                            <p class="help-block">Photos for NamNam Product, Only .jpg, .png, .gif, .bmp allowed.</p>
                        </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('description', 'Product Description') }}
                            {{ Form::textarea('description', null, array('class' => 'form-control', 'placeholder' => 'Product Description')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('url', 'URL') }}
                            {{ Form::text('url', null, array('class' => 'form-control', 'placeholder' => 'URL')) }}
                        </div>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary btn-block')) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop

@section('page-specific-js')
<script type="text/javascript">
	$( "textarea" ).jqte();
</script>
@stop