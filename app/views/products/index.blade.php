@extends('cms.templates.chumper')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Product View All</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Product Settings <a href="{{ URL::route('manage.products.create') }}" class="btn btn-default"> Add New</a></h1>
        </div><!-- /.col-lg-12 -->
        <div class="col-lg-12">
            {{ Datatable::table()
                ->addColumn('Title', 'Description', 'URL', 'Product Thumbnail', '', '')
                ->setUrl(route('ajax.product'))
                ->setOptions('dom', '<"row"<"col-lg-6 col-md-6"f><"col-lg-6 col-md-6"<"pull-right"l>>><t><ip>')
                ->setId('datatable')
                ->render('cms.templates.sections.datatable') }}
        </div>
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop