<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    @section('title')
    <title>Namnam CMS</title>
    @show

    <!-- Bootstrap Core CSS -->
    {{ HTML::style('cms/css/bootstrap.min.css') }}

    <!-- MetisMenu CSS -->
    {{ HTML::style('cms/css/plugins/metisMenu/metisMenu.min.css') }}

    <!-- Custom CSS -->
    {{ HTML::style('cms/css/sb-admin-2.css') }}

    <!-- Custom Fonts -->
    {{ HTML::style('cms/font-awesome-4.1.0/css/font-awesome.min.css') }}

    <!-- Custom Namnam CSS -->
    {{ HTML::style('cms/css/namnam-custom.css') }}

    <script type="text/javascript" src="cms/highslide/highslide.js"></script>
    <link rel="stylesheet" type="text/css" href="cms/highslide/highslide.css" />
    <script type="text/javascript">
        // override Highslide settings here
        // instead of editing the highslide.js file
        hs.graphicsDir = 'cms/highslide/graphics/';
    </script>
</head>

<body>
    {{ HTML::script('cms/js/jquery.js') }}
    {{ HTML::script('cms/js/bootstrap.min.js') }}
    {{ HTML::script('cms/js/plugins/dataTables/jquery.dataTables.js') }}
    {{ HTML::script('cms/js/plugins/dataTables/dataTables.bootstrap.js') }}

    {{ HTML::script('cms/js/plugins/metisMenu/metisMenu.min.js') }}
    {{ HTML::script('cms/js/sb-admin-2.js') }}
    {{ HTML::script('cms/js/plugins/jscolor/jscolor.js') }}

    <div id="wrapper">
        <!-- Navigation -->
        @include('cms.templates.sections.topbar')
        @include('cms.templates.sections.sidebar')

        <!-- Page Content -->
        <div id="page-wrapper">
            @yield('content')
        </div><!-- /#page-wrapper -->
    </div><!-- /#wrapper -->


    @yield('page-specific-js')
</body>

</html>
