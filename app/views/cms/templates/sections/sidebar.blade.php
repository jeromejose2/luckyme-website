
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="{{ URL::route('manage.home') }}"><i class="fa fa-home fa-fw"></i> Home</a>
                </li>
                @if (Auth::user()->isSuperAdmin())
                    <li>
                        <a href="{{ URL::route('manage.users.index') }}"><i class="fa fa-group fa-fw"></i> Users<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ URL::route('manage.users.index') }}">View All</a>
                            </li>
                            <li>
                                <a href="{{ URL::route('manage.users.create') }}">Add New</a>
                            </li>
                        </ul><!-- /.nav-second-level -->
                    </li>
                @endif

                <li>
                    <a href="{{ URL::route('manage.dear-bossing.index') }}"><i class="fa fa-briefcase fa-fw"></i> Dear Bossing<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ URL::route('manage.dear-bossing.index') }}">View All</a>
                        </li>
                        <li  class="{{ Auth::user()->isVisitor() ? 'hidden' : '' }}">
                            <a href="{{ URL::route('manage.dear-bossing.create') }}">Add New</a>
                        </li>
                    </ul><!-- /.nav-second-level -->
                </li>

                <li>
                    <a href="{{ URL::route('manage.settings.index') }}"><i class="fa fa-gear fa-fw"></i> Settings<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ URL::route('manage.settings.index') }}">View All</a>
                        </li>
                        <li class="{{ Auth::user()->isVisitor() ? 'hidden' : '' }}">
                            <a href="{{ URL::route('manage.settings.create') }}">Add New</a>
                        </li>
                    </ul><!-- /.nav-second-level -->
                </li>

                <li>
                    <a href="{{ URL::route('manage.homepage-sliders.index') }}"><i class="fa fa-sliders fa-fw"></i> Homepage Sliders<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ URL::route('manage.homepage-sliders.index') }}">View All</a>
                        </li>
                        <li class="{{ Auth::user()->isVisitor() ? 'hidden' : '' }}">
                            <a href="{{ URL::route('manage.homepage-sliders.create') }}">Add New</a>
                        </li>
                    </ul><!-- /.nav-second-level -->
                </li>

                <li>
                    <a href="{{ URL::route('manage.homepage-sliders.index') }}"><i class="fa fa-cubes fa-fw"></i> Products<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ URL::route('manage.products.index') }}">View All</a>
                        </li>
                        <li class="{{ Auth::user()->isVisitor() ? 'hidden' : '' }}">
                            <a href="{{ URL::route('manage.products.create') }}">Add New</a>
                        </li>
                    </ul><!-- /.nav-second-level -->
                </li>

                <li>
                    <a href="{{ URL::route('manage.homepage-sliders.index') }}"><i class="fa fa-list fa-fw"></i> Recipes<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ URL::route('manage.recipes.index') }}">View All</a>
                        </li>
                        <li class="{{ Auth::user()->isVisitor() ? 'hidden' : '' }}">
                            <a href="{{ URL::route('manage.recipes.create') }}">Add New</a>
                        </li>
                    </ul><!-- /.nav-second-level -->
                </li>

                <li>
                    <a href="{{ URL::route('manage.categories.index') }}"><i class="fa fa-tags fa-fw"></i> Categories<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ URL::route('manage.categories.index') }}">View All</a>
                        </li>
                        <li class="{{ Auth::user()->isVisitor() ? 'hidden' : '' }}">
                            <a href="{{ URL::route('manage.categories.create') }}">Add New</a>
                        </li>
                    </ul><!-- /.nav-second-level -->
                </li>

            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>