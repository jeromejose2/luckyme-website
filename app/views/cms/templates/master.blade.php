<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    @section('title')
    <title>Namnam CMS</title>
    @show

    <!-- Bootstrap Core CSS -->
    {{ HTML::style('cms/css/bootstrap.min.css') }}

    <!-- MetisMenu CSS -->
    {{ HTML::style('cms/css/plugins/metisMenu/metisMenu.min.css') }}

    <!-- Custom CSS -->
    {{ HTML::style('cms/css/sb-admin-2.css') }}
    
    <!-- jqte CSS -->
    {{ HTML::style('cms/js/plugins/jqte/jqte.css') }}

    <!-- Custom Fonts -->
    {{ HTML::style('cms/font-awesome-4.1.0/css/font-awesome.min.css') }}

    <!-- Custom Namnam CSS -->
    {{ HTML::style('cms/css/namnam-custom.css') }}

    <script type="text/javascript" src="cms/highslide/highslide.js"></script>
<link rel="stylesheet" type="text/css" href="cms/highslide/highslide.css" />
<script type="text/javascript">
    // override Highslide settings here
    // instead of editing the highslide.js file
    hs.graphicsDir = 'cms/highslide/graphics/';
</script>

</head>

<body>

    <div id="wrapper">
        @if(Auth::guest())
            <!-- Login Content -->
            @yield('login')
        @else
            <!-- Navigation -->
            @include('cms.templates.sections.topbar')
            @include('cms.templates.sections.sidebar')

            <!-- Page Content -->
            <div id="page-wrapper">
                @yield('content')
            </div>
            <!-- /#page-wrapper -->
        @endif
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    {{ HTML::script('cms/js/jquery.js') }}

    <!-- Bootstrap Core JavaScript -->
    {{ HTML::script('cms/js/bootstrap.min.js') }}

    <!-- Metis Menu Plugin JavaScript -->
    {{ HTML::script('cms/js/plugins/metisMenu/metisMenu.min.js') }}

    <!-- Custom Theme JavaScript -->
    {{ HTML::script('cms/js/sb-admin-2.js') }}

    {{ HTML::script('cms/js/plugins/jscolor/jscolor.js') }}
    
     {{ HTML::script('cms/js/plugins/jqte/jqte.js') }}

    @yield('page-specific-js')

</body>

</html>
