<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>{{$data['subject']}}</h2>

        <div>
            {{ $data['name'] }}<br>
            {{ $data['contact'] }}<br>
            {{ $data['address'] }}<br>
            <br>
            {{ $data['message']}}
        </div>
    </body>
</html>
