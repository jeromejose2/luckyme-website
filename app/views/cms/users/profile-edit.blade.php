@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - View User</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

@if($errors->has())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <div>{{ $error }}</div>
        @endforeach
    </div>
@endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Profile - {{ $user->name }}</h1>
            {{ Form::open(array('route' => array('manage.users.update', $user->id), 'method' => 'put')) }}
                <div class="row">
                    <div class="col-md-4 col-md-offset-1">
                        <div class="form-group">
                            {{ Form::label('email', 'Email') }}
                            {{ Form::email('email', $user->email, array('class' => 'form-control', 'placeholder' => 'E-mail')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('password', 'Password') }}
                            {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('password_confirmation', 'Confirm Password') }}
                            {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Confirm Password')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('access_level', 'Access Level') }}
                            {{ Form::select('access_level', array('super admin' => 'Super Admin', 'admin' => 'Admin', 'visitor' => 'Visitor'), $user->access_level, array('class' => 'form-control')); }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', $user->name, array('class' => 'form-control', 'placeholder' => 'First Name Last Name')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('username', 'User name') }}
                            {{ Form::text('username', $user->username, array('class' => 'form-control', 'placeholder' => 'User name')) }}
                        </div>
                    </div>
                </div><!--/.row -->
                <div class="row">
                    <div class="col-md-2 col-md-offset-1">
                        {{ Form::submit('Update', array('class' => 'btn btn-primary btn-block')) }}
                    </div>
                    <div class="col-md-2">
                        {{ HTML::linkRoute('manage.profile', 'Cancel', null, array('class' => 'btn btn-default btn-block')) }}
                    </div>
                </div><!--/.row -->
              {{ Form::close() }}
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop