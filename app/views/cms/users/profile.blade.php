@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Profile</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Profile - {{ $user->name }}</h1>
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    <div class="form-group">
                        {{ Form::label('email', 'Email') }}
                        {{ Form::email('email', $user->email, array('class' => 'form-control', 'placeholder' => 'E-mail', 'readonly' => 'readonly')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('access_level', 'Access Level') }}
                        {{ Form::text('access_level', $user->access_level, array('class' => 'form-control', 'placeholder' => 'Access Level', 'readonly' => 'readonly')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('name', 'Name') }}
                        {{ Form::text('name', $user->name, array('class' => 'form-control', 'placeholder' => 'First Name Last Name', 'readonly' => 'readonly')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('username', 'User name') }}
                        {{ Form::text('username', $user->username, array('class' => 'form-control', 'placeholder' => 'User name', 'readonly' => 'readonly')) }}
                    </div>
                </div>
            </div><!--/.row -->
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    {{ HTML::linkRoute('manage.profile-edit', 'Edit Profile', null, array('class' => 'btn btn-primary btn-block')) }}
                </div>
            </div><!--/.row -->
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop