@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Edit Dear Bossing</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Dear Bossing</h1>
            {{ Form::open(array('route' => array('manage.dear-bossing.update', $dearbossing->id), 'method' => 'put')) }}
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                            <div class="col-md-8">
                                {{ Form::label('title', 'Title') }}
                                {{ Form::text('title', $dearbossing->title, array('class' => 'form-control', 'placeholder' => $dearbossing->title)) }}
                            </div>
                            <div class="col-md-4">
                                {{-- Form::label('episode_id', 'Episode ID') --}}
                                {{-- Form::text('episode_id', $dearbossing->episode_id, array('class' => 'form-control', 'placeholder' => $dearbossing->episode_id)) --}}
                                {{ Form::hidden('episode_id', $dearbossing->episode_id)}}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                {{ Form::label('Article', 'Article') }}
                                {{ Form::textarea('article', $dearbossing->article, array('class' => 'form-control', 'row' => '5', 'placeholder' => $dearbossing->article)) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                {{ Form::label('youtube_url', 'YouTube URL') }}
                                {{ Form::text('youtube_url', $dearbossing->youtube_url, array('class' => 'form-control', 'placeholder' => $dearbossing->youtube_url)) }}
                            </div>
                            <div class="col-md-6">
                                {{ Form::label('email_address', 'E-mail Address') }}
                                {{ Form::email('email_address', $dearbossing->email_address, array('class' => 'form-control', 'placeholder' => $dearbossing->email_address)) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            {{ '<b>Is Active?</b>' }}
                            <div class="radio">
                              <label>
                                {{ Form::radio('is_active', 1, $dearbossing->is_active ? true : false) }}
                                Yes
                              </label>
                              <label>
                                {{ Form::radio('is_active', 0, $dearbossing->is_active ? false : true) }}
                                No
                              </label>
                            </div>
                        </div>
                    </div>
                </div><!--/.row-->
                <div class="row">
                    <div class="col-md-2 col-md-offset-1">
                        {{ Form::submit('Update', array('class' => 'btn btn-primary btn-block')) }}
                    </div>
                    <div class="col-md-2 col-md-offset-6">
                        {{ HTML::linkRoute('manage.dear-bossing.show', 'Cancel', $dearbossing->id, array('class' => 'btn btn-default btn-block')) }}
                    </div>
                </div><!--/.row -->
            {{ Form::close() }}
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop