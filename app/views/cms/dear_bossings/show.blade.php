@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Dear Bossing</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dear Bossing</h1>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="form-group">
                        <div class="col-md-8">
                            {{ Form::label('title', 'Title') }}
                            {{ Form::text('title', $dearbossing->title, array('class' => 'form-control', 'placeholder' => 'Title', 'readonly' => 'readonly')) }}
                        </div>
                        <div class="col-md-4">
                            {{ Form::label('episode_id', 'Episode ID') }}
                            {{ Form::text('episode_id', $dearbossing->episode_id, array('class' => 'form-control', 'placeholder' => 'Episode ID', 'readonly' => 'readonly')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            {{ Form::label('Article', 'Article') }}
                            {{ Form::textarea('article', $dearbossing->article, array('class' => 'form-control', 'row' => '5', 'readonly' => 'readonly')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            {{ Form::label('youtube_url', 'YouTube URL') }}
                            {{ Form::text('youtube_url', $dearbossing->youtube_url, array('class' => 'form-control', 'placeholder' => 'YouTube URL', 'readonly' => 'readonly')) }}
                        </div>
                        <div class="col-md-6">
                            {{ Form::label('email_address', 'E-mail Address') }}
                            {{ Form::email('email_address', $dearbossing->email_address, array('class' => 'form-control', 'placeholder' => 'E-mail Address', 'readonly' => 'readonly')) }}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                        {{ '<b>Is Active?</b>' }}
                        <br>
                        @if ($dearbossing->is_active)
                            <i class="fa fa-check-circle fa-fw fa-2x text-success"></i>
                        @else
                            <i class="fa fa-times-circle fa-fw fa-2x text-danger"></i>
                        @endif
                    </div>
                    @if ( ! Auth::user()->isVisitor())
                        <div class="col-md-2">
                            {{ HTML::linkRoute('manage.dear-bossing.edit', 'Edit', $dearbossing->id, array('class' => 'btn btn-primary btn-block')) }}
                        </div>
                        <div class="col-md-2 col-md-offset-8">
                            {{ Form::open(array('route' => array('manage.dear-bossing.destroy', $dearbossing->id), 'method' => 'delete')) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-block')) }}
                            {{ Form::close() }}
                        </div>
                    @endif

                    @if (Auth::user()->isVisitor())
                        <div class="col-md-2">
                            {{ HTML::linkRoute('manage.dear-bossing.index', 'Back', null, array('class' => 'btn btn-default btn-block')) }}
                        </div>
                    @endif
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop