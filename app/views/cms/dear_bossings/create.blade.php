@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Create Dear Bossing</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create Dear Bossing</h1>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    {{ Form::open(array('route' => 'manage.dear-bossing.store')) }}
                        <div class="form-group">
                            <div class="col-md-8">
                                {{ Form::label('title', 'Title') }}
                                {{ Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Title')) }}
                            </div>
                            <div class="col-md-4">
                                {{-- Form::label('episode_id', 'Episode ID') --}}
                                {{-- Form::text('episode_id', null, array('class' => 'form-control', 'placeholder' => 'Episode ID')) --}}
                                {{ Form::hidden('episode_id', '') }}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                {{ Form::label('Article', 'Article') }}
                                {{ Form::textarea('article', null, array('class' => 'form-control', 'row' => '5')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                {{ Form::label('youtube_url', 'YouTube URL') }}
                                {{ Form::text('youtube_url', null, array('class' => 'form-control', 'placeholder' => 'YouTube URL')) }}
                            </div>
                            <div class="col-md-6">
                                {{ Form::label('email_address', 'E-mail Address') }}
                                {{ Form::email('email_address', null, array('class' => 'form-control', 'placeholder' => 'E-mail Address')) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            {{ '<b>Is Active?</b>' }}
                            <div class="radio">
                              <label>
                                {{ Form::radio('is_active', 1, true) }}
                                Yes
                              </label>
                              <label>
                                {{ Form::radio('is_active', 0, false) }}
                                No
                              </label>
                            </div>
                            {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop