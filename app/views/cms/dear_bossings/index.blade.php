@extends('cms.templates.chumper')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Dear Bossing</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dear Bossing <a href="{{ URL::route('manage.dear-bossing.create') }}" class="btn btn-default"> Add New</a></h1>
        </div><!-- /.col-lg-12 -->
        <div class="col-lg-12">
            Total entries: {{$active + $inactive}}. Total active: {{$active}}. Total inactive: {{$inactive}}.
        </div>
        <div class="col-lg-12">
            @if (Auth::user()->isVisitor())
                {{ Datatable::table()
                    ->addColumn('Title', 'Article', 'YouTube', 'Active', 'E-mail', '')
                    ->setUrl(route('ajax.dear-bossing'))
                    ->setOptions('dom', '<"row"<"col-lg-6 col-md-6"f><"col-lg-6 col-md-6"<"pull-right"l>>><t><ip>')
                    ->setId('datatable')
                    ->render('cms.templates.sections.datatable') }}
            @else
                {{ Datatable::table()
                    ->addColumn('Title', 'Article', 'YouTube', 'Active', 'E-mail', '', '', '')
                    ->setUrl(route('ajax.dear-bossing'))
                    ->setOptions('dom', '<"row"<"col-lg-6 col-md-6"f><"col-lg-6 col-md-6"<"pull-right"l>>><t><ip>')
                    ->setId('datatable')
                    ->render('cms.templates.sections.datatable') }}
            @endif
        </div>
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop