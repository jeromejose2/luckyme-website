@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Homepage Slider</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Homepage Slider</h1>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h3>Content Type: {{{ $homepageslider->content_type }}}</h3>
                        <div class="form-group">
                            <div class="col-md-4">
                                {{ Form::label('slider_id', 'Slider ID') }}
                                {{ Form::text('slider_id', $homepageslider->slider_id, array('class' => 'form-control', 'placeholder' => 'Slider ID', 'readonly' => 'readonly')) }}
                            </div>
                    @if($homepageslider->content_type === 'Image')
                            <div class="col-md-8">
                                {{ Form::label('photo', 'Photo') }}
                                {{ Form::text('photo', $homepageslider->photo, array('class' => 'form-control', 'placeholder' => 'Photo', 'readonly' => 'readonly')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12"><hr></div>
                            <div class="col-md-6">
                                {{ Form::label('button1_url_copyline1', 'Button 1 URL Copyline 1') }}
                                {{ Form::text('button1_url_copyline1', $homepageslider->button1_url_copyline1, array('class' => 'form-control', 'placeholder' => 'Button 1 URL Copyline 1', 'readonly' => 'readonly')) }}
                            </div>
                            <div class="col-md-6">
                                {{ Form::label('button1_url_bg', 'Button 1 URL BG') }}
                                {{ Form::text('button1_url_bg', $homepageslider->button1_url_bg, array('class' => 'form-control', 'placeholder' => 'Button 1 URL BG', 'readonly' => 'readonly')) }}
                            </div>
                            <div class="col-md-6">
                                {{ Form::label('button1_url_copyline2', 'Button 1 URL Copyline 2') }}
                                {{ Form::text('button1_url_copyline2', $homepageslider->button1_url_copyline2, array('class' => 'form-control', 'placeholder' => 'Button 1 URL Copyline 2', 'readonly' => 'readonly')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12"><hr></div>
                            <div class="col-md-6">
                                {{ Form::label('button2_url_copyline1', 'Button 2 URL Copyline 1') }}
                                {{ Form::text('button2_url_copyline1', $homepageslider->button2_url_copyline1, array('class' => 'form-control', 'placeholder' => 'Button 2 URL Copyline 1', 'readonly' => 'readonly')) }}
                            </div>
                            <div class="col-md-6">
                                {{ Form::label('button2_url_bg', 'Button 2 URL BG') }}
                                {{ Form::text('button2_url_bg', $homepageslider->button2_url_bg, array('class' => 'form-control', 'placeholder' => 'Button 2 URL BG', 'readonly' => 'readonly')) }}
                            </div>
                            <div class="col-md-6">
                                {{ Form::label('button2_url_copyline2', 'Button 2 URL Copyline 2') }}
                                {{ Form::text('button2_url_copyline2', $homepageslider->button2_url_copyline2, array('class' => 'form-control', 'placeholder' => 'Button 2 URL Copyline 2', 'readonly' => 'readonly')) }}
                            </div>
                        </div>

                    @elseif($homepageslider->content_type ==='Video')
                        <div class="form-group">
                            <div class="col-md-12"><hr></div>
                            <div class="col-md-6">
                                {{ Form::label('bg_video_url', 'BG Video URL') }}
                                {{ Form::text('bg_video_url', $homepageslider->bg_video_url, array('class' => 'form-control', 'placeholder' => 'BG Video URL', 'readonly' => 'readonly')) }}
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <div class="col-md-12"><hr></div>
                            @if ( ! Auth::user()->isVisitor())
                                <div class="col-md-2">
                                    {{ HTML::linkRoute('manage.homepage-sliders.edit', 'Edit', $homepageslider->id, array('class' => 'btn btn-primary btn-block')) }}
                                </div>
                                <div class="col-md-2 col-md-offset-8">
                                    {{ Form::open(array('route' => array('manage.homepage-sliders.destroy', $homepageslider->id), 'method' => 'delete')) }}
                                    {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-block')) }}
                                    {{ Form::close() }}
                                </div>
                            @endif

                            @if (Auth::user()->isVisitor())
                                <div class="col-md-2">
                                    {{ HTML::linkRoute('manage.homepage-sliders.index', 'Back', null, array('class' => 'btn btn-default btn-block')) }}
                                </div>
                            @endif
                    </div>
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop