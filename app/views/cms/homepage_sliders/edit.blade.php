@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Edit Homepage Slider</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Homepage Slider</h1>
            {{ Form::open(array('route' => array('manage.homepage-sliders.update', $homepageslider->id), 'method' => 'put', 'files' => true)) }}
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                            <h3>Content Type: {{ $homepageslider->content_type }}</h3>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        {{ Form::label('slider_id', 'Slider Order') }}
                                        (Arrangement of slides)
                                        {{ Form::text('slider_id', $homepageslider->slider_id, array('class' => 'form-control', 'placeholder' => $homepageslider->slider_id)) }}
                                    </div>
                            @if($homepageslider->content_type === 'Image')
                                     <div class="col-md-8"><br />
                                        {{ Form::label('photo', 'Photo') }}
                                        {{ Form::file('slider-photo') }}
                                        <p class="help-block">Photos for Image, Only .jpg, .png, .gif, .bmp allowed.</p>
                                        {{ HTML::image($homepageslider->photo,"namnam",array('style'=>'width: 100px; height: 100px;')) }}
                                        <br />
                                        <br />
                                        {{ Form::label('mobile-photo', 'Mobile Photo') }}
                                        {{ Form::file('mobile-photo') }}
                                        <p class="help-block">Photos for Image, Only .jpg, .png, .gif, .bmp allowed.</p>
                                        {{ HTML::image($homepageslider->mobile_photo,"namnam",array('style'=>'width: 100px; height: 100px;')) }}
                                     </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12"><hr></div>
                                    <div class="col-md-6">
                                        {{ Form::label('button1_url_copyline1', 'Button 1 URL Copyline 1') }}
                                        {{ Form::text('button1_url_copyline1', $homepageslider->button1_url_copyline1, array('class' => 'form-control', 'placeholder' => $homepageslider->button1_url_copyline1)) }}
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::label('button1_url_copyline2', 'Button 1 URL Copyline 2') }}
                                        {{ Form::text('button1_url_copyline2', $homepageslider->button1_url_copyline2, array('class' => 'form-control', 'placeholder' => $homepageslider->button1_url_copyline2)) }}
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::label('button1_fontcolor_label', 'Button 1 Font Color') }}
                                        {{ Form::text('button1_fontcolor', $homepageslider->button1_fontcolor, array('class' => 'form-control color {adjust: false}', 'id' => 'fctext1'))}}
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::label('button1_bgcolor_label', 'Button 1 BG Color') }}
                                        {{ Form::text('button1_bgcolor', $homepageslider->button1_bgcolor, array('class' => 'form-control color {adjust: false}', 'id' => 'bgtext1'))}}

                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::label('button1_url_bg', 'Button 1 URL') }}
                                        {{ Form::text('button1_url_bg', $homepageslider->button1_url_bg, array('class' => 'form-control', 'placeholder' => $homepageslider->button1_url_bg)) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12"><hr></div>
                                    <div class="col-md-6">
                                        {{ Form::label('button2_url_copyline1', 'Button 2 URL Copyline 1') }}
                                        {{ Form::text('button2_url_copyline1', $homepageslider->button2_url_copyline1, array('class' => 'form-control', 'placeholder' => $homepageslider->button2_url_copyline1)) }}
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::label('button2_url_copyline2', 'Button 2 URL Copyline 2') }}
                                        {{ Form::text('button2_url_copyline2', $homepageslider->button2_url_copyline2, array('class' => 'form-control', 'placeholder' => $homepageslider->button2_url_copyline2)) }}
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::label('button2_fontcolor_label', 'Button 2 Font Color') }}
                                        {{ Form::text('button2_fontcolor', $homepageslider->button2_fontcolor, array('class' => 'form-control color {adjust : false}', 'id' => 'fctext2'))}}
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::label('button2_bgcolor_label', 'Button 2 BG Color') }}
                                        {{ Form::text('button2_bgcolor', $homepageslider->button2_bgcolor, array('class' => 'form-control color {adjust: false}', 'id' => 'bgtext2'))}}
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::label('button2_url_bg', 'Button 2 URL') }}
                                        {{ Form::text('button2_url_bg', $homepageslider->button2_url_bg, array('class' => 'form-control', 'placeholder' => $homepageslider->button2_url_bg)) }}
                                    </div>
                                </div>
                            @elseif($homepageslider->content_type === 'Video')
                                <div class="form-group">
                                    <div class="col-md-12"><hr></div>
                                    <div class="col-md-6">
                                        {{ Form::label('bg_video_url', 'BG Video URL') }}

                                        @if($homepageslider->bg_video_url == '')
                                        {{ Form::text('bg_video_url',null, array('class' => 'form-control', 'placeholder' => HomepageSlider::getYouTubeURLFromID($homepageslider->bg_video_url))) }}
                                        @else
                                         {{ Form::text('bg_video_url', HomepageSlider::getYouTubeURLFromID($homepageslider->bg_video_url) , array('class' => 'form-control', 'placeholder' => HomepageSlider::getYouTubeURLFromID($homepageslider->bg_video_url))) }}
                                        @endif
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                <div class="col-md-12"><hr></div>
                                <div class="col-md-2">
                                    {{ Form::submit('Update', array('class' => 'btn btn-primary btn-block')) }}
                                </div>
                                <div class="col-md-2 col-md-offset-8">
                                    {{ HTML::linkRoute('manage.homepage-sliders.show', 'Cancel', $homepageslider->id, array('class' => 'btn btn-default btn-block')) }}
                                </div>
                            </div>
                    </div>
                </div><!--/.row -->
            {{ Form::close() }}
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop

