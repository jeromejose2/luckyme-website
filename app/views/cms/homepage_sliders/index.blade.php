@extends('cms.templates.chumper')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Homepage Sliders</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Homepage Sliders <a href="{{ URL::route('manage.homepage-sliders.create') }}" class="btn btn-default"> Add New</a></h1>
        </div><!-- /.col-lg-12 -->
        <div class="col-lg-12">
            @if (Auth::user()->isVisitor())
                {{ Datatable::table()
                    ->addColumn('Slider ID', 'Preview', '')
                    ->setUrl(route('ajax.homepage-slider'))
                    ->setOptions('dom', '<"row"<"col-lg-6 col-md-6"f><"col-lg-6 col-md-6"<"pull-right"l>>><t><ip>')
                    ->setId('datatable')
                    ->render('cms.templates.sections.datatable') }}
            @else
                {{ Datatable::table()
                    ->addColumn('Slider ID', 'Preview', '', '', '')
                    ->setUrl(route('ajax.homepage-slider'))
                    ->setOptions('dom', '<"row"<"col-lg-6 col-md-6"f><"col-lg-6 col-md-6"<"pull-right"l>>><t><ip>')
                    ->setId('datatable')
                    ->render('cms.templates.sections.datatable') }}
            @endif
        </div>
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop