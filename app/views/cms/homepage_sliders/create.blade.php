@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Create Homepage Slider</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create Homepage Slider</h1>
            <div class="row">
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    {{ Form::open(array('route' => 'manage.homepage-sliders.store', 'files' => true)) }}
                        <div class="form-group">
                            <div class="col-md-4">
                                {{ Form::label('slider_id', 'Slider Order') }}
                                (Arrangement of slides)
                                {{ Form::text('slider_id', null, array('class' => 'form-control', 'placeholder' => 'Slider Order : 1')) }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <hr>
                                <div role="tabpanel">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#video" aria-controls="video" role="tab" data-toggle="tab">Add Video</a></li>
                                        <li role="presentation"><a href="#image" aria-controls="image" role="tab" data-toggle="tab">Add Image</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="video">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    {{ Form::label('bg_video_url', 'BG Video URL') }}
                                                    {{ Form::text('bg_video_url', null, array('class' => 'form-control', 'placeholder' => 'BG Video URL')) }}
                                                </div>
                                            </div>
                                        </div><!--/video -->
                                        <div role="tabpanel" class="tab-pane" id="image">
                                            <div class="col-md-8">
                                                {{ Form::label('photo', 'Photo') }}
                                                {{ Form::file('slider-photo') }}
                                                <p class="help-block">Photos for Image, Only .jpg, .png, .gif, .bmp allowed.</p>
                                                {{ Form::label('mobile-photo', 'Mobile Photo') }}
                                                {{ Form::file('mobile-photo') }}
                                                <p class="help-block">Photos for Image, Only .jpg, .png, .gif, .bmp allowed.</p>
                                                <hr>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    {{ Form::label('button1_url_copyline1', 'Button 1 URL Copyline 1') }}
                                                    {{ Form::text('button1_url_copyline1', null, array('class' => 'form-control', 'placeholder' => 'Button 1 URL Copyline 1')) }}
                                                </div>
                                                <div class="col-md-6">
                                                    {{ Form::label('button1_url_copyline2', 'Button 1 URL Copyline 2') }}
                                                    {{ Form::text('button1_url_copyline2', null, array('class' => 'form-control', 'placeholder' => 'Button 1 URL Copyline 2')) }}
                                                </div>
                                                <div class="col-md-6">
                                                    {{ Form::label('button1_fontcolor_label', 'Button 1 Font Color') }}
                                                    {{ Form::text('button1_fontcolor', null, array('class' => 'form-control color {adjust: false}', 'id' => 'fctext1'))}}
                                                </div>
                                                <div class="col-md-6">
                                                    {{ Form::label('button1_bgcolor_label', 'Button 1 BG Color') }}
                                                    {{ Form::text('button1_bgcolor', null, array('class' => 'form-control color {adjust: false}', 'id' => 'bgtext1'))}}
                                                </div>
                                                <div class="col-md-6">
                                                    {{ Form::label('button1_url_bg', 'Button 1 URL') }}
                                                    {{ Form::text('button1_url_bg', null, array('class' => 'form-control', 'placeholder' => 'Button 1 URL')) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12"><hr></div>
                                                <div class="col-md-6">
                                                    {{ Form::label('button2_url_copyline1', 'Button 2 URL Copyline 1') }}
                                                    {{ Form::text('button2_url_copyline1', null, array('class' => 'form-control', 'placeholder' => 'Button 2 URL Copyline 1')) }}
                                                </div>
                                                <div class="col-md-6">
                                                    {{ Form::label('button2_url_copyline2', 'Button 2 URL Copyline 2') }}
                                                    {{ Form::text('button2_url_copyline2', null, array('class' => 'form-control', 'placeholder' => 'Button 2 URL Copyline 2')) }}
                                                </div>
                                                <div class="col-md-6">
                                                    {{ Form::label('button2_fontcolor_label', 'Button 2 Font Color') }}
                                                    {{ Form::text('button2_fontcolor', null, array('class' => 'form-control color {adjust: false}', 'id' => 'fctext2'))}}
                                                </div>
                                                <div class="col-md-6">
                                                    {{ Form::label('button2_bgcolor_label', 'Button 2 BG Color') }}
                                                    {{ Form::text('button2_bgcolor', null, array('class' => 'form-control color {adjust: false}', 'id' => 'bgtext2'))}}
                                                </div>
                                                <div class="col-md-6">
                                                    {{ Form::label('button2_url_bg', 'Button 2 URL') }}
                                                    {{ Form::text('button2_url_bg', null, array('class' => 'form-control', 'placeholder' => 'Button 2 URL')) }}
                                                </div>
                                            </div>
                                        </div><!--/image -->
                                    </div><!--/.tab-content-->
                                </div><!--/.tabpanel -->
                            </div><!--/.col-lg-12 -->
                        </div><!--/.row-->


                        <div class="form-group">
                            <div class="col-md-12"><hr></div>
                            <div class="col-md-2">
                                {{ Form::submit('Save', array('class' => 'btn btn-primary btn-block')) }}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop
