@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Create Settings</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create Settings</h1>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    {{ Form::open(array('route' => 'manage.settings.store')) }}
                        <div class="form-group">
                            {{ Form::label('type', 'Type') }}
                            {{ Form::text('type', null, array('class' => 'form-control', 'placeholder' => 'Type')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('content', 'Content') }}
                            {{ Form::textarea('content', null, array('class' => 'form-control', 'rows' => '5'))}}
                        </div>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop