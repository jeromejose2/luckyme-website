@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Edit Settings</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Settings</h1>
            {{ Form::open(array('route' => array('manage.settings.update', $setting->id), 'method' => 'put', 'files' => true)) }}
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                            {{ Form::label('type', 'Type') }}
                            {{ Form::text('type', $setting->type, array('class' => 'form-control', 'placeholder' => $setting->type, 'readonly' => 'readonly')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('content', 'Content') }}
                            {{ Form::textarea('content', $setting->content, array('class' => 'form-control', 'rows' => '5', 'placeholder' => $setting->content))}}
                        </div>
                    </div>
                </div><!--/.row-->
                @if($setting->type == 'site_bgm')
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-1">
                        {{ Form::file('new-bgm') }}
                        <p class="help-block">Upload new background music.</p>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-md-2 col-md-offset-1">
                        {{ Form::submit('Update', array('class' => 'btn btn-primary btn-block')) }}
                    </div>
                    <div class="col-md-2 col-md-offset-6">
                        {{ HTML::linkRoute('manage.settings.show', 'Cancel', $setting->id, array('class' => 'btn btn-default btn-block')) }}
                    </div>
                </div><!--/.row -->
            {{ Form::close() }}
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop

@section('page-specific-js')
	@if(in_array($setting->type, array('chef_content', 'artist_content')))
		<script type="text/javascript">
			$( "textarea" ).jqte();
		</script>
	@endif
@stop