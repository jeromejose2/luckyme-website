@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Create Settings</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">View Settings</h1>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="form-group">
                        {{ Form::label('type', 'Type') }}
                        {{ Form::text('type', $setting->type, array('class' => 'form-control', 'placeholder' => 'Type', 'readonly' => 'readonly')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('content', 'Content') }}
                        {{ Form::textarea('content', $setting->content, array('class' => 'form-control', 'rows' => '5', 'readonly' => 'readonly'))}}
                    </div>
                </div>
            </div><!--/.row-->
            <div class="row">
                @if ( ! Auth::user()->isVisitor())
                    <div class="col-md-2 col-md-offset-1 ">
                        {{ HTML::linkRoute('manage.settings.edit', 'Edit', $setting->id, array('class' => 'btn btn-primary btn-block')) }}
                    </div>
                @endif

                @if (Auth::user()->isVisitor())
                    <div class="col-md-2 col-md-offset-1">
                        {{ HTML::linkRoute('manage.settings.index', 'Back', null, array('class' => 'btn btn-default btn-block')) }}
                    </div>
                @endif
            </div><!--/.row -->

        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop