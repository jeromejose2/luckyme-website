@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Login</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('login')

<div class="row">
</div>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Please Sign In</h3>
            </div>
            <div class="panel-body">
                {{ Form::open(array('route' => 'manage.postLogin', 'role' => 'form')) }}
                    <fieldset>
                        <div class="form-group">
                            @if($errors->has())
                                <div class="col-md-12">
                                    <div class="alert alert-danger" role="alert">
                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        {{ $errors->first() }}
                                    </div>
                                </div><!--/.col-md-12 -->
                            @endif
                            {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'E-mail', 'type' => 'email', 'autofocus' => 'autofocus')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                        </div>
                        <div class="checkbox">
                            <label for='remember_me'>
                                {{ Form::checkbox('remember', 'Remember Me', false, array('id' => 'remember_me')) }}
                                Remember Me
                            </label>
                        </div>
                        <!-- Change this to a button or input when using this as a form -->
                        {{ Form::submit('Login', array('class' => 'btn btn-lg btn-success btn-block')) }}
                    </fieldset>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@stop