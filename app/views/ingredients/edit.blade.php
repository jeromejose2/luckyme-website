@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Edit Ingredient</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

    @if($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Ingredient</h1>
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                       {{ Form::open(array('route' => array('manage.ingredients.update',$ingredient->id),'method' => 'put','files' => true)) }}
                        <div class="form-group">
                            {{-- Form::label('product', 'Select Recipe', array('class' => 'label-control')) --}}
                            {{-- Form::select('recipe_id', $recipes, $ingredient->recipe_id, array('class' => 'form-control')) --}}
                            {{ Form::hidden('recipe_id', $ingredient->recipe_id)}}
                            {{ Form::label('recipe_name', $recipe->recipe_name)}}
                        </div>
                        <div class="form-group">
                            {{ Form::label('qty', 'Quantity') }}
                            {{ Form::text('qty', $ingredient->qty, array('class' => 'form-control', 'placeholder' => 'Quantity')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name',$ingredient->name, array('class' => 'form-control', 'placeholder' => 'Name of Ingredients')) }}
                        </div>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary btn-block')) }}
                        @if(isset($recipe_id))
                            {{Form::hidden('recipe_id', $recipe_id)}}
                        @endif
                    {{ Form::close() }}
                </div>
            </div>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop