@extends('cms.templates.master')

{{-- TITLE : templates.master --}}
@section('title')
<title>Namnam CMS - Ingredient View All</title>
@stop

{{-- CONTENT AREA : templates.master --}}
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Ingredient Settings <a href="{{ URL::route('manage.ingredients.create') }}" class="btn btn-default"> Add New</a></h1>
        </div><!-- /.col-lg-12 -->
        <div class="col-lg-12">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Recipe Name</th>
                        <th>Quantity</th>
                        <th>Ingredient Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
   				 @foreach ($ingredients as $ingredient)
                        <tr>
                            <td>{{ Recipe::find($ingredient->recipe_id)->recipe_name }}</td>
                            <td>{{ $ingredient->qty }}</td>
                            <td>{{ $ingredient->name }}</td>
                            <td>{{ HTML::linkRoute('manage.ingredients.edit','Edit', $ingredient->id  )}}</td>
                        </tr>
  				  @endforeach
                </tbody>
            </table>
        </div>
    </div><!-- /.row -->
</div><!-- /.container-fluid -->

@stop