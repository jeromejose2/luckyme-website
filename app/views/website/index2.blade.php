@include('website.header')
<!-- variable for js -->
@if($recipe != 'none')
<span id="recipe_var" data-id="{{ $recipe->id }}"></span>
@else
<span id="recipe_var" data-id="0"></span>
@endif
<!-- <div class="doodle-black"> -->
    
<!-- #1 -->
<section class="page" id="slider">

    <div id="owl-slider" class="owl-carousel owl-theme">

        @foreach($sliders as $key => $slider)
            @if($slider->content_type == 'Image')
                <div class="item">
                    @if($slider->photo == '')
                      <div class="slide-bg" id="slide{{$key}}">
                    @else
                      <div class="slide-bg" id="slide{{$key}}" data-desktop="{{ url('/').'/'.$slider->photo }}" data-mobile="{{ url('/').'/'.$slider->mobile_photo }}" style="background-image:url({{ URL::asset($slider->photo)}});">
                    @endif
                         <div class="btn-holder">
                            @if($slider->button1_bgcolor == '')
                             <a href="{{ $slider->button1_url_bg }}" class="btn-nam2x-original">
                            @else
                             <a href="{{ $slider->button1_url_bg }}" class="btn-nam2x-original"  style="background-color: #{{$slider->button1_bgcolor;}}">
                            @endif
                            @if($slider->button1_fontcolor == '')
                                <h2>{{ $slider->button1_url_copyline1 }}</h2>
                                <p>{{ $slider->button1_url_copyline2 }}</p>
                            @else
                                <h2 style="color: #{{$slider->button1_fontcolor}};">{{ $slider->button1_url_copyline1 }}</h2>
                                <p style="color: #{{$slider->button1_fontcolor}};">{{ $slider->button1_url_copyline2 }}</p>
                            @endif
                             </a>
                            @if($slider->button2_bgcolor == '')
                                <a href="{{ $slider->button2_url_bg }}" class="btn-nam2x-tomato">
                            @else
                                <a href="{{ $slider->button2_url_bg }}" class="btn-nam2x-tomato" style=" background-color: #{{$slider->button2_bgcolor;}}">
                            @endif
                            @if($slider->button2_fontcolor == '')
                                <h2>{{ $slider->button2_url_copyline1 }}</h2>
                                <p>{{ $slider->button2_url_copyline2 }}</p>
                            @else
                                <h2 style="color: #{{$slider->button2_fontcolor}};">{{ $slider->button2_url_copyline1 }}</h2>
                                <p style="color: #{{$slider->button2_fontcolor}};">{{ $slider->button2_url_copyline2 }}</p>
                            @endif
                             </a>
                         </div>
                     </div>
                 </div>
             @else
                <div class="item">
                     <iframe width="100%" height="100%" id="player{{$key}}"
                        src="https://www.youtube.com/embed/{{ $slider->bg_video_url }}?enablejsapi=1">
                    </iframe>

                </div>
             @endif
        @endforeach
    </div>
    
</section>

<!-- #2 -->
<section class="page" id="featured-recipe">
    
    <div class="f-outer-wrapper">
        
        <div class="inner-wrapper">

            @include('website.inner-recipe-section')

        </div>
    
    </div>

</section>



<!-- #3 -->
<section class="page" id="recipe">

    <div class="container">

        <div class="category-holder">
            
            <!--filter-->
            <div class="filter-holder">

                <div class="select-wrapper">
                    {{ Form::select('filter_select', $categories, 'All', array('id' => 'filter_select'))}}
                </div>

            </div>
            
            <!--info-->

            <div class="clearfix"></div>

            <!--tabs-->
            <div class="tabs-holder">

                <div class="tab-nav-all"><a href="#recipe" class="a-tab-nav active" id="a_all">All Recipes</a></div>
                
                <div class="tab-nav-org"><a href="#recipe" class="a-tab-nav" id="a_org">Original Recipes</a></div>
                
                <div class="tab-nav-tomato"><a href="#recipe" class="a-tab-nav" id="a_tomato">Tomato Recipes</a></div>
                
                <div class="clearfix"></div>

                <div class="content-holder">

                    <span data-preload="{{ URL::asset('website/assets/img/preload.gif')}}" id="preload"></span>

                    <div class="item" id="recipe1">
                    </div>

                    <div class="item" id="recipe2">
                    </div>

                    <div class="item" id="recipe3">
                    </div>

                    <div class="item"  id="recipe4">
                    </div>

                    <div class="item"  id="recipe5">
                    </div>

                    <div class="item" id="recipe6">
                    </div>

                    <div class="item" id="recipe7">
                    </div>

                    <div class="item" id="recipe8">
                    </div>

                <div class="clearfix"></div>

            <!-- content-holder end -->
            </div>

            <div class="legend cf">

                <!-- <div class="list">

                    <img src="{{ URL::asset('website/assets/img/mark-bossing.png')}}">

                    <p>Bossing Approved</p>

                </div> -->

                <div class="list" style="width:auto;">

                    <img class="lazyload" data-src="{{ URL::asset('website/assets/img/mark-chef.png')}}">

                    <p>Chef&#39;s Recommendation</p>

                </div>

                <div class="list" style="width:auto;">

                    <img class="lazyload" data-src="{{ URL::asset('website/assets/img/mark-kids.png')}}">

                    <p>Kid's Choice</p>

                </div>

            </div>

            

        <!-- tabs-holder end -->
        </div>

        
        

        <div class="pagination">
            <a href="#recipe"  id="p_prev" class="p_disabled">&lt;&lt;prev</a>
            <ul>
                <li><a href="#recipe" class="p_num active" id="p_1">1</a></li>
                <li><a href="#recipe" class="p_num p_disabled" id="p_2">2</a></li>
                <li><a href="#recipe" class="p_num p_disabled" id="p_3">3</a></li>
                <li><a href="#recipe" class="p_num p_disabled" id="p_4">4</a></li>
                <li><a href="#recipe" class="p_num p_disabled" id="p_5">5</a></li>
            </ul>
            <a href="#recipe" class="p_disabled"  id="p_next">next&gt;&gt;</a>
        </div>

        <div class="header-info">
            <a href="#" class="anchor-whitebg">Have a recipe to share? <em>Email us at luckymenamnam@mondenissin.com.tph <br/>or Snail mail it to us at 9 Sheridan St., Guerrero Compound, <br/>Brgy. Buayang Bato, Mandaluyong City. </em></a>
        </div>

        <!-- category-holder end -->
        </div>

    </div>

</section>






<!-- #4 -->
<section class="page" id="products">

    <div class="doodle-wrapper">
        <div class="product-wrapper">
        
            <h2>{{ $settings['product_header'] }}</h2>
            
            <div class="img-wrapper">
            
                <img class="lazyload" data-src="{{ URL::asset($settings['product_header_img'])}}">
            
            </div>

            <div class="category-wrapper">

                <div class="p-nam-original">

                    <h3>{{ $products[0]->title }}</h3>
					
                    <p class="custom-font">{{ nl2br($products[0]->description)}}</p>
                
                    
                    <a href="#recipe" class="btn-nam2x-original" onclick="ajaxRecipe('originalrecipes', 1); refreshPagination();">

                        <h2>{{ $products[0]->title }}</h2>

                        <p>for all-around seasoning</p>

                    </a>

                </div>

                <div class="p-nam-tomato">

                    <h3>{{ $products[1]->title }}</h3>

                    <p class="custom-font">{{ nl2br($products[1]->description) }}</p>
                    
                    
                    <a href="#recipe" class="btn-nam2x-tomato" onclick="ajaxRecipe('tomatorecipes', 1); refreshPagination();">

                        <h2>{{ $products[1]->title }}</h2>

                        <p>for tomato dishes</p>

                    </a>

                </div>

                <div class="clearfix"></div>

            </div>
        
        </div>

    </div>

</section>

<div class="clearfix"></div>

<!-- #5 -->
<section class="page" id="namTV">
    
    <div class="namnam-wrapper">
        
        <div class="content-wrapper">

            <div class="left-chef-wrapper">

                <img class="lazyload" data-src="{{ URL::asset('website/assets/img/namTv-left-img.png')}}">

                <img class="lazyload" data-src="{{ URL::asset('website/assets/img/cheft-pop.png')}}">

            
            </div>

            <div class="right-artist-wrapper">

                <img class="lazyload" data-src="{{ URL::asset('website/assets/img/namTv-right-img.png')}}">


                <img class="lazyload" data-src="{{ URL::asset('website/assets/img/artist-pop.png')}}">


            </div>
        
        </div>

        <div class="btn-wrapper">
            <a href="javascript:void(0);" id="chef-corner" class="btn-nam2x-original"> <!-- onclick="lytebox.open({url:'chefbabespopup.php', blurClose:false })" -->

                <h2>{{ $settings['chef_btn']}}</h2>

                <p>{{ $settings['chef_btn_desc']}}</p>

            </a>

            <a href="javascript:void(0);" id="m-tips" class="btn-nam2x-tomato"> <!-- onclick="lytebox.open({url:'artistmpopup.php', blurClose:false })" -->

                <h2>{{ $settings['artist_btn']}}</h2>

                <p>{{ $settings['artist_btn_desc']}}</p>

            </a>

            <div class="clearfix"></div>
        </div>

        <a target="_blank" href="{{ $settings['video_btn_url']}}" id="watchVideo" class="btn-nam2x-tomato x-btn-small">

            <h2>{{ $settings['video_btn_top']}}</h2>

            <p>{{ $settings['video_btn_bottom']}}</p>

        </a>
        
    </div>

</section>
@include('website.footer')

<script>
    $('.owl-prev, .owl-next').addClass('lazyload');
</script>