<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ $settings['site_title'] }}</title>
  
  <!--added favicon -->
  <link rel="icon" href="{{ URL::asset('website/assets/img/favicon.ico') }}" type="image/x-icon" />

  <!--
  Disable Bootstrap
  <link href="/website/assets/vendors/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link href="/website/assets/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->

  <!--fonts -->
  <!-- <link href="{{ URL::asset('website/assets/fonts/fontLib/allfonts.css')}}" rel="stylesheet"> -->

  <!--owl -->
  <!-- <link href="{{ URL::asset('website/assets/vendors/owl-carousel/owl.theme.css') }}" rel="stylesheet"> -->
  <!-- <link href="{{ URL::asset('website/assets/vendors/owl-carousel/owl.carousel.css') }}" rel="stylesheet"> -->
  <!-- <link href="{{ URL::asset('website/assets/vendors/owl-carousel/owl.transitions.css') }}" rel="stylesheet"> -->

  <!-- lytebox -->
  <!-- <link href="{{ URL::asset('website/assets/vendors/lytebox/css/lytebox.css') }}" rel="stylesheet"> -->

  <!-- sharre -->
  <!-- <link href="{{ URL::asset('website/assets/vendors/sharrre/css/sharrre.css') }}" rel="stylesheet"> -->
  
  <!-- <link rel="stylesheet" href="{{ URL::asset('website/assets/css/dmdt.css') }}"> -->
  
  <link rel="stylesheet" href="{{ URL::asset('website/assets/css/main.min.css') }}">

  @if($recipe != 'none')
    <meta property="og:image" content="{{ URL::asset($recipe->photo) }}"/>
    <meta property="og:url" content="{{ URL::to('?recipe_id='.$recipe->id) }}" />
    <meta property="og:title" content="Lucky Me! NamNam recipe for {{ $recipe->recipe_name }}" />
    <meta property="og:description" content="{{ $recipe->description }}" />

  @else
    <meta property="og:image" content="{{ URL::asset('website/assets/img/namtv-popup-artist.png') }}"/>
    <meta property="og:url" content="{{ URL::to('') }}" />
    <meta property="og:title" content="Lucky Me! NamNam" />
    <meta property="og:description" content="A website for Lucky Me! NamNam" />
  @endif
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body>
<!--[if lt IE 10]>
  <p class="browsehappy">
    You are using an <strong>outdated</strong> browser. Please
    <a href="http://browsehappy.com/">upgrade your browser</a> to improve
    your experience.
  </p>
<![endif]-->
<header>

      <div class="nav">

          <h1 class="n-logo">
              <a href="{{ URL::to('')}}">
              <img class="lazyload" data-src="{{ URL::asset('website/assets/img/site-logo.png') }}">
              </a>
          </h1>

          <a href="javascript:void(0);" class="btn-sound icons-sound-on" id="musicBtn"></a>
          <audio src="{{ $settings['site_bgm']}}" id="musicPlayer" autoplay="true">
          </audio>

          <a class="nav-toggle" href="javascript:void(0);"><span class="bar"></span><span class="bar"></span><span class="bar"></span></a>

          <div class="social">

              <span>Share</span>

              <a href="javascript:void(0);" class="btn-fb" onclick="fb_sharer('https://www.facebook.com/sharer/sharer.php?u={{ URL::to('')}}', 'Share on Facebook')"> </a>

              <a class="btn-tw"
                href="https://twitter.com/intent/tweet?text=Check+out+this+site!+{{ Setting::bitly(URL::to('')) }}"
                target="_blank"></a>
                <span data-via="{{ $settings['social_twitter_via'] }}" id="tw_via"> </span>

              <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

          </div>

          <div class="clearfix"></div>
      </div>

      <!--collapse-->
      <div class="container">

        <div class="nav-collapse">

          <a href="#slider">HOME</a>

          <a class="special" href="javascript:void(0);">RECIPES</a>

            <a class="indent" href="#featured-recipe">FEATURED RECIPES</a>

            <a class="indent" href="#recipe">ALL RECIPES</a>

          <a href="#products">PRODUCTS</a>

          <a href="#namTV">NAMNAM TV</a>

        </div>

    </div>

</header>

<!-- floating navigation -->
<div class="main-nav">
    <ul>
        <li><a href="#slider" class="active"><span class="icons-home lazyload"></span><span class="icons-txt-home"></span></a></li>
       
        <li class="innernav-recipe"><a href="javascript:void(0);"><span class="icons-recipe lazyload"></span><span class="icons-txt-recipe"></span></a>
           <div class="nav-recipe">
              <a href="#featured-recipe"><span class="icons-starlee lazyload"></span> <span class="icons-txt-featured-recipes"></span></a>
              <a href="#recipe"><span class="icons-allrecipe lazyload"></span><span class="icons-txt-all-recipes"></span></a>
           </div>
        </li>

        <li><a href="#products"><span class="icons-product lazyload"></span><span class="icons-txt-product"></span></a></li>
        <li><a href="#namTV"><span class="icons-namnamTv lazyload"></span><span class="icons-txt-namnamTV"></span></a></li>
    </ul>
</div>
<div id="fb-root"></div>