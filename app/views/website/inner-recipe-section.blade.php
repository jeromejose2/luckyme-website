<div class="recipe-preview">
                
        <h2>Featured Recipe</h2>
        
        <div class="img-wrapper">
            @if($featured == 'none')
            <img class="featured-img lazyload" data-src="">
            @else
            <img class="featured-img lazyload" data-src="{{ $featured->photo }}">
            @endif
        </div>
        @if($featured_product == 'none')
        <img class="swakpack lazyload" data-src="{{URL::asset('website/assets/img/pack-namnam.png')}}">
        @else
        <img class="swakpack lazyload" data-src="{{URL::asset($featured_product->namnam_product_shot)}}">
        @endif
        @if($featured == 'none')
            <p class="title">None yet.</p>
        @else
            <p class="title">{{ $featured->recipe_name }} <br> <span style="color:black; font-size: small;"> Dish by : {{ $featured->dish_by }} </span></p>
        @endif
        
        <div class="heart-vote">
            @if($featured == 'none')
                <div class="ratings" data-ratings="0">
            @else
                <div class="ratings" data-ratings="{{ $average }}" data-id="{{ $featured->id }}">
            @endif
                <button><span></span></button>
                <button><span></span></button>
                <button><span></span></button>
                <button><span></span></button>
                <button><span></span></button>
            </div>
		<div class="lbl-votes"><span id="featured-total-votes">{{ $total_votes }}</span> votes</div>
        </div>

        @if($featured == 'none')
            <p class="label"><span class="icons-recipe-time"></span>Preparation Time: <span class="data-time">0</span></p>
            <!-- <br /> -->
            <p class="label"><span class="icons-recipe-people"></span>Serving Size: <span class="data-serve">0</span></p>
        @else
            <p class="label"><span class="icons-recipe-time"></span>Preparation Time: <span class="data-time">{{ $featured->prep_time }}</span></p>
            <!-- <br /> -->
            <p class="label"><span class="icons-recipe-people"></span>Serving Size: <span class="data-serve">{{ $featured->serving_size }}</span></p>
        @endif
</div>

<div class="recipe-steps">

    <div id="inner-content-div">

            @if($featured != 'none')
            	<div class="custom-font">
            		<p>
	            		{{ $featured->description }}
	            	</p>
                </div>
            @endif

            @if($ingredients == 'none')
                No ingredients yet.
            @else
                <p>
                    <b class="recipe-header"> What You Need </b> <br>
                    @foreach($ingredients as $i)
                         {{nl2br($i->qty. ' ' .$i->name)}} <br>
                    @endforeach
                </p>
            @endif

            @if($procedures == 'none')
                No procedures yet.
            @else
                <p>
                    <b class="recipe-header"> How to Cook </b> <br>
                    @foreach($procedures as $p)
                         {{nl2br($p->name)}} <br>
                         {{nl2br($p->item)}} <br> <br>
                    @endforeach
                </p>
            @endif
    </div>

</div>

<div class="side-footer">

    <div id="popupshare" class="cf">
        @if($featured == 'none')

            <div class="facebook" data-url="{{ URL::to('')}}" data-text="Check out this NamNam recipe!"></div>
            <div class="twitter" data-url="{{ Setting::bitly(URL::to('')) }}" data-text="Check out this featured {{ '' }} recipe with NamNam!"></div>
        @else
            <div class="facebook" data-url="{{ URL::to('/?recipe_id='.$featured->id)}}" data-text="Check out this NamNam recipe!"></div>
            <div class="twitter" data-url="{{ Setting::bitly(URL::to('/?recipe_id='.$featured->id)) }}" data-text="Check out this featured {{ $featured->recipe_name }} recipe with NamNam!"></div>
        @endif

        <!--new button-->
        <a target="_blank" href="{{ URL::to('recipe/print/preview', array('id' => $featured->id)) }}" class="btn blue-print">Print</a>
        <a href="{{ URL::to('recipe/print', array('id' => $featured->id)) }}" class="btn blue-download">Download</a>
    </div>

    <a href="#" class="header-info"> Have a recipe to share?<br/> <em>Email us at luckymenamnam@mondenissin.com.tph<br/> or Snail mail it to us at 9 Sheridan St., Guerrero Compound,<br/> Brgy. Buayang Bato, Mandaluyong City. </em></a>

</div>

<div class="clearfix"></div>
