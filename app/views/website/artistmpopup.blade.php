<div class="popup-wrapper">

    <a href="javascript:void(0);" class="btn-close" onclick="lytebox.close()">&times;</a>

    <div class="popup-outer noimg">
        
        <div class="popup-inner">

            <div class="left-img-wrapper">

                <img class="lazyload" data-src="{{ URL::asset('website/assets/img/namTv-right-img.png')}}">


                <img class="artist-bubbles lazyload" data-src="{{ URL::asset('website/assets/img/artist-pop.png')}}">
            

            </div>

            <div class="information">
                @if($title == 'none')
                <h2>No assigned title to artist_title</h2>
                @else
                <h2>{{ $title->content }}</h2>
                @endif
                <div id="info-inner-content-div">
                    @if($content == 'none')
                    <p>
                        {{nl2br('Sorry, there are no entries yet. Stay tuned!')}}
                    </p>
                    @else
                    	<div class="custom-font">
		                    <p>
		                        {{ nl2br($content->content) }}
		                    </p>
		                </div>
                    @endif
                </div>

                <a href="#">View Lucky Me! NamNam TVC’s >></a>
            </div>

            <div class="clearfix"></div>
        </div>

    </div>

</div>

<script type="text/javascript">
    $(function(){
        $('#info-inner-content-div').slimScroll({
            position: 'right',
            height: '450px',
            railVisible: true,
            alwaysVisible: true
        });
    });
</script>