<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ $site_title }} - {{ $data->recipe_name }}</title>
  
  <!--
  Disable Bootstrap
  <link href="/website/assets/vendors/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link href="/website/assets/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->

  <!--fonts -->
  <link href="{{ URL::asset('website/assets/fonts/fontLib/allfonts.css') }}" rel="stylesheet">

  <!--owl -->
  <link href="{{ URL::asset('website/assets/vendors/owl-carousel/owl.theme.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('website/assets/vendors/owl-carousel/carousel.theme.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('website/assets/vendors/owl-carousel/transitions.theme.css') }}" rel="stylesheet">
  <!-- lytebox -->
  <link href="{{ URL::asset('website/assets/vendors/lytebox/css/lytebox.css')}}" rel="stylesheet">

  <!-- sharre -->
  <link href="{{ URL::asset('website/assets/vendors/sharrre/css/sharrre.css') }}" rel="stylesheet">

  <link rel="stylesheet" href="{{ URL::asset('website/assets/css/main.min.css')}}">
  <link rel="stylesheet" href="{{ URL::asset('website/assets/css/dmdt.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

  <meta property="og:image" content="{{ URL::asset($data->photo) }}"/>
  <meta property="og:image:secure_url" content="{{ URL::asset($data->photo) }}" />
  <meta property="og:title" content="NamNam Recipe for {{ $data->recipe_name }}" />
  <meta property="og:description" content="Check out this NamNam recipe!" />
</head>
<body>
    <img class="lazyload" data-src="{{ URL::asset($data->photo) }}" style="display:none;" />
<!--[if lt IE 10]>
  <p class="browsehappy">
    You are using an <strong>outdated</strong> browser. Please
    <a href="http://browsehappy.com/">upgrade your browser</a> to improve
    your experience.
  </p>
<![endif]-->
<div id="fb-root">
    </div>
<header>

      <div class="nav">
          <span></span>
          <h1 class="n-logo">
              <a href="{{ URL::to('/') }}">
              <img class="lazyload" data-src="{{ URL::asset('website/assets/img/site-logo.png')}}">
              </a>
          </h1>

          <a href="javascript:void(0);" class="btn-sound icons-sound-on" id="musicBtn"></a>
          <audio src="{{ $bgm   }}" id="musicPlayer" autoplay="true">
          </audio>

          <a class="nav-toggle" href="javascript:void(0);"><span class="bar"></span><span class="bar"></span><span class="bar"></span></a>

          <div class="social">

              <span>Share</span>

              <a class="btn-tw"
                href="https://twitter.com/intent/tweet?text=Check+out+this+site!+{{ Setting::bitly(URL::to('/'))  }}"
                target="_blank"></a>
                <span data-via="{{ $via }}" id="tw_via"> </span>

              <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
              <a href="javascript:void(0);" class="btn-fb" onclick="fb_sharer('https://www.facebook.com/sharer/sharer.php?u={{ URL::to('')}}', 'Share on Facebook')"> </a>

          </div>

          <div class="clearfix"></div>
      </div>

</header>

<!-- #2 -->
<section class="page" id="featured-recipe" style="padding-top: 100px;">
    
    <div class="f-outer-wrapper">
        
        <div class="inner-wrapper">

            <div class="recipe-preview">
                            
                    <h2>Featured Recipe</h2>
                    
                    <div class="img-wrapper">
                        
                        <img class="featured-img lazyload" data-src="{{ URL::asset($data->photo) }}">
                    
                    </div>
                    
                    <img class="swakpack lazyload" data-src="{{ URL::asset($product->namnam_product_shot) }}">
                    
                    <p class="title">{{ $data->recipe_name }}</p>
                    
                    <div class="heart-vote">

                        <div class="ratings" data-ratings="{{ $average }}" data-id="{{ $data->id }}">
                            <button><span></span></button>
                            <button><span></span></button>
                            <button><span></span></button>
                            <button><span></span></button>
                            <button><span></span></button>
                        </div>

                    </div>

                    <p class="label"><span class="icons-recipe-time"></span>Preparation Time: <span class="data-time">{{ $data->prep_time }}</span></p>
                    <!-- <br /> -->
                    <p class="label"><span class="icons-recipe-people"></span>Serving Size: <span class="data-serve"></span></p>
            </div>

            <div class="recipe-steps">

                <div id="inner-content-div">

                    <p>
                        Ingredients : <br>
                        @foreach($ingredients as $i)
                             {{ $i->name }} - {{ $i->qty }} <br>
                        @endforeach
                    </p>
                    <p>
                        Procedure : <br>
                        @foreach($procedures as $p)
                             {{ $p->name }} - {{ $p->item }} <br>
                        @endforeach
                    </p>

                </div>

            </div>

            <div class="side-footer">

                <div id="popupshare">
                  <div class="facebook" data-url="{{ URL::to('recipe/'.$data->id) }}" data-text="Check out this NamNam recipe!"></div>
                  <div class="twitter" data-url="{{ Setting::bitly(URL::to('recipe/'.$data->id)) }}" data-text="Check out this {{ $data->recipe_name }} recipe with NamNam!"></div>
                </div>

                <a href="#" class="header-info"> Have a recipe to share?<br/> <em>Email us at luckymenamnam@mondenissin.com.tph<br/> or Snail mail it to us at 9 Sheridan St., Guerrero Compound,<br/> Brgy. Buayang Bato, Mandaluyong City. </em></a>

            </div>

            <div class="clearfix"></div>

        </div>
    
    </div>

    <span data-via="{{ $via }}" id="via_tw"> </span>
</section>

<script src="{{ URL::asset('website/assets/vendors/jquery/jquery.js') }}"></script>
<script src="{{ URL::asset('website/assets/vendors/owl-carousel/owl.carousel.js') }}"></script>
<!-- slim-scroll -->
<script src="{{ URL::asset('website/assets/vendors/slimScroll/jquery.slimscroll.min.js') }}"></script>

<!-- lytebox -->
<script src="{{ URL::asset('website/assets/vendors/lytebox/js/lytebox.2.3.js') }}"></script>

<!-- waypoint -->
<script src="{{ URL::asset('website/assets/vendors/imakewebthings/waypoints.js') }}"></script> 

<!-- 
Disable bootstrap
<script src="website/assets/vendors/bootstrap/js/bootstrap.js"></script> -->

<!-- sharrre -->
<script src="{{URL::asset('website/assets/vendors/sharrre/js/jquery.sharrre.js')}}"></script>

<script src="{{ URL::asset('website/assets/js/app.min.js') }}"></script>
<script src="{{ URL::asset('website/assets/js/dmdt.js') }}"></script>

<script type="text/javascript">
    var tw_via = $("#via_tw").data('via');
    $('.twitter').sharrre({
      share: {
        twitter: true
      },
      template: '<a href="#"><div class="share"><span></span>Tweet</div></a>',
      enableHover: false,
      enableTracking: true,
      buttons: { twitter: {via: tw_via}},
      click: function(api, options){
        api.simulateClick();
        api.openPopup('twitter');
      }
    });
    $('.facebook').sharrre({
      share: {
        facebook: true
      },
      template: '<a href="#"><div class="share"><span></span>Share</div></a>',
      enableHover: false,
      enableTracking: true,
      click: function(api, options){
        api.simulateClick();
        api.openPopup('facebook');
      }
    });

    $(function(){
        $('#recipe-inner-content-div').slimScroll({
            position: 'right',
            height: '545px',
            railVisible: true,
            alwaysVisible: true
        });
    });

    function innerRatings(){
        $('#inner-ratings button').removeClass('active');
        $('#inner-ratings button span').css({width:'100%'});

        $('#inner-ratings button').each(function(){
            var me = $(this);
            var index = me.index('#inner-ratings button') + 1;
            var ratings = me.parent().data('ratings');

            if( index <= Math.ceil(ratings) ){
                me.addClass('active');
            }

            if( index == Math.ceil(ratings) ){
                var decimal =  Math.round(100 - ((index - ratings) * 100)) ;
                me.children().css({width: decimal + '%'});
            }
        });
    }

    $(function(){
        innerRatings();
        $('#inner-ratings button').click(function(){
            var me = $(this);
            var index = me.index('#inner-ratings button') + 1;
            $('#inner-ratings').data('ratings',index);
            innerRatings();
        });
    });
</script>

</body>

</html>
