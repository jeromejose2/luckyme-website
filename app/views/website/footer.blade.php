<footer id="footer">
    <a href="javascript:void(0);" class="footer-toggle"></a>
    <span class="line-des"></span>
    
<div class="container">
    
    <div class="send-feedback">
        <label>SEND FEEDBACK</label>
        <div class="typeform-holder">
            <!-- put typeform here -->
                {{ Form::open(array('route' => 'website.mail', 'method' => 'POST','class' => 'type-form', 'id' => 'typeform-footer'))}}
                <ul>

                    <li>
                        <label>1
                            &nbsp;<span class="icons-f-arrow"></span>&nbsp; First, Please tell us your name</label>
                        <input type="text" data-error="Oops! Please input your first name to continue" name="name" class="f-required" tabindex="-1">
                    </li>

                    <li class="item">
                        <label>2
                            &nbsp;<span class="icons-f-arrow"></span>&nbsp; What is your email address</label>
                        <input type="text" data-error="Oops! Please input your email to continue" name="email" class="f-required f-email" tabindex="-1">
                    </li>

                    <li class="item">
                        <label>
                            &nbsp;<span class="icons-f-arrow"></span>&nbsp; What is your contact number</label>
                        <input type="text" data-error="Oops! Please input your contact number to continue" name="contact" class="f-required" maxlength="11" tabindex="-1">
                    </li>

                    <li class="item">
                        <label>4
                           &nbsp;<span class="icons-f-arrow"></span>&nbsp; What is your address</label>
                        <input type="text" name="address" data-error="Oops! Please input your address to continue" class="f-required" tabindex="-1">
                    </li>

                    <li class="item">
                        <label>5
                            &nbsp;<span class="icons-f-arrow"></span>&nbsp; Subject</label>
                        <input type="text" data-error="Oops! Please enter a subject to continue" name="subject" class="f-required" tabindex="-1">
                    </li>

                    <li class="item">
                        <label>6
                            &nbsp;<span class="icons-f-arrow"></span>&nbsp; Message</label>
                        <textarea data-error="Oops! Please enter your message to continue" name="message" class="f-required" id="tteesstt" tabindex="-1"></textarea>
                    </li>
                </ul>
            {{ Form::close() }}

            <!-- end typeform -->
            <!-- <button class="btn btn-default type-form-btn" onclick="tform.prev()">BACK </button> -->
            <button class="type-form-btn" onclick="tform.next()">CONTINUE </button>

        <!-- put typeform here end -->
        </div>

    </div>

    <div class="connect-with-us">
        <label>CONNECT WITH US</label>
        <ul class="f-social">
            <li><a target="_blank" href="{{ $settings['social_facebook'] }}"><span class="icons-sitemap-fb"></span>Facebook</a></li>
            <li><a target="_blank" href="{{ $settings['social_youtube'] }}"><span class="icons-sitemap-yt"></span>YouTube</a></li>
        </ul>
    </div>

    <div class="trademark">
        <div class="the-company">
            <h2 class="logo"></h2>
            <p class="mond-border">{{ $settings['corporation'] }}</p>
            <p>{{  $settings['phone']  }}</p>
            <p>{{  $settings['address'] }}</p>
        </div>
    </div>

    <div class="clearfix"></div>
</div>

<div class="darkblock">
    <div class="container">
        <p>{{  $settings['copyright']  }}</p>
    </div>
</div>

<!--doodle-black end
</div>-->

</footer>

<script src="website/assets/vendors/jquery/jquery.js"></script>
<script src="website/assets/vendors/owl-carousel/owl.carousel.js"></script>
<!-- slim-scroll -->
<script src="website/assets/vendors/slimScroll/jquery.slimscroll.min.js"></script>
<!-- lytebox -->
<script src="website/assets/vendors/lytebox/js/lytebox.2.3.js"></script>
<!-- waypoint -->
<script src="website/assets/vendors/imakewebthings/waypoints.js"></script> 
<!-- sharrre -->
<script src="website/assets/vendors/sharrre/js/jquery.sharrre.js"></script>
<!-- 
Disable bootstrap
<script src="website/assets/vendors/bootstrap/js/bootstrap.js"></script> -->

<script src="website/assets/vendors/lazysize.js"></script>
<script src="website/assets/js/dmdt.js"></script>
<script src="website/assets/js/app.min.js"></script>
<!-- <script src="website/assets/js/script.js"></script> -->


<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 ga('create', 'UA-42684485-35', 'auto');
 ga('send', 'pageview');
</script>

<script>
var tw_via = $("#tw_via").data('via');
$('.twitter').sharrre({
  share: {
    twitter: true
  },
  template: '<a href="#"><div class="share"><span></span>Tweet</div></a>',
  enableHover: false,
  enableTracking: true,
  buttons: { twitter: {via: tw_via}},
  click: function(api, options){
    api.simulateClick();
    api.openPopup('twitter');
  }
});
$('.facebook').sharrre({
  share: {
    facebook: true
  },
  template: '<a href="#"><div class="share"><span></span>Share</div></a>',
  enableHover: false,
  enableTracking: true,
  click: function(api, options){
    api.simulateClick();
    api.openPopup('facebook');
  }
});


//lazyload
document.addEventListener('lazybeforeunveil', function(e){
    var bg = e.target.getAttribute('data-bg');
    if(bg){
        e.target.style.backgroundImage = 'url(' + bg + ')';
    }
});

</script>

</body>

</html>
