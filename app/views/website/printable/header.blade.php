<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--   <meta name="viewport" content="width=device-width, initial-scale=1"> -->
  <title>{{ $recipe['recipe_name'] }} Recipe</title>
  
  <link href="{{ URL::asset('website/assets/vendors/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('website/assets/vendors/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('website/assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('website/assets/fonts/fontLib/allfonts.css') }}" rel="stylesheet">
  
  <!-- build:css assets/css/main.min.css -->
  <link href="{{ URL::asset('website/assets/css/printable.min.css') }}" rel="stylesheet">
  <!-- /build -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body>
<!--[if lt IE 10]>
  <p class="browsehappy">
    You are using an <strong>outdated</strong> browser. Please
    <a href="http://browsehappy.com/">upgrade your browser</a> to improve
    your experience.
  </p>
<![endif]-->
