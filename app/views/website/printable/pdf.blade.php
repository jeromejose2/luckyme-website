<!DOCTYPE html>
<html>
	<head>
		 <title>{{ $recipe['recipe_name'] }} Recipe</title>
		 
		 <link href="{{ URL::asset('website/assets/css/print.css') }}" rel="stylesheet">
	</head>
	
	<body>
		<div id="main-container">
			<div class="namnam-box"></div>
			
			<div id="recipe-container">
				<div class="imgholder" style="background-image:url({{ URL::asset($recipe->photo) }});"></div>
		
				<table>
					<tr>
						<td class="recipe-name" colspan="2">{{ $recipe->recipe_name }}</td>
					</tr>
					<tr valign="top">
						<td width="50%">
							<table>
								<tr><th colspan="2">What You Need</th></tr>
								
								@foreach($ingredients as $i)
						        	<tr>
										<td width="50%">{{ $i->qty }}</td>
										<td width="50%">{{ $i->name }}</td>
									</tr>
						        @endforeach
							</table>
						</td>
						<td width="50%">
							<table>
								<tr><th colspan="2">How to Cook</th></tr>
								
								<tr>
									<td>
										@foreach($procedures as $p)
						                	{{nl2br($p->name)}}. {{nl2br($p->item)}} <br/><br/>
						                @endforeach
									</td>
								<tr>
				
							</table>
						</td>
					</tr>
		  		</table>
			
			<div class="copyright">
	  			Copyright 2014, Monde Nissin Corporation. All rights reserved.
	  		</div>
		</div>
	</body>
</html>