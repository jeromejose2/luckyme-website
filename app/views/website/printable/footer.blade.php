
  <script src="{{ URL::asset('website/assets/vendors/jquery/jquery.js') }}"></script>
  <script src="{{ URL::asset('website/assets/js/printable.min.js') }}"></script>
	
  <div class="sticky-link">	
	  	<div class="container clearpads">
	  		<a href="{{ URL::to('/') . '#recipe' }}">Back to <b>Recipes Page</b></a>
	  	</div>
  	</div>
</body>
</html>
