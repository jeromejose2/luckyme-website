@include('website.printable.header')
	
	<div class="btn-wrapper">
		<div class="container cf head-no-pad">
			
			<a href="#" class="btn remove-image cf"><span class="icon-x">&times;</span><span class="remove-image-label">Remove Image</span></a>
			
			<a onclick="window.print()" class="btn print-recipe cf">
				<span class="icon-wrapper">
					<span class="icon-print"></span>
				</span>
				<span class="caption">
					<span>Print Recipe</span>
				</span>
			</a>
			
		</div>
	</div>

	<div class="container bordered cf">

		<div class="namnam-box"></div>
		<img class="print-namnam-box" src="{{ URL::asset('website/assets/img/namnam-footer-logo.jpg') }}">

		<div class="content cf">
  			<div class="imgholder" style="background-image:url({{ URL::asset($recipe->photo) }});">
  				<img class="print-image-container" src="{{ URL::asset($recipe->photo) }}">
  			</div>
  			
  			<span class="recipe-name">{{ $recipe['recipe_name'] }}</span>
  			
  			<div class="text-editor-wrap">
			<table class="ingre">
				<tr><th colspan='3'>What You Need</th></tr>
				
				@foreach($ingredients as $i)
		        	<tr>
						<td>{{ $i->qty }}</td>
						<td>{{ $i->name }}</td>
					</tr>
		        @endforeach
			</table>
			
			<table class="steps">
				<tr><th colspan='3'>How to Cook</th></tr>
				
				<tr>
					<td>
						@foreach($procedures as $p)
		                	{{nl2br($p->name)}}. {{nl2br($p->item)}} <br/><br/>
		                @endforeach
					</td>
				<tr>

			</table>

  			<!--end of text-editor-wrap-->	
  			</div>
  		
  		<!--end of content-->
  		</div>

  		<div class="copyright">
  			Copyright 2014, Monde Nissin Corporation. All rights reserved.
  		</div>

  	</div>

@include('website.printable.footer')
