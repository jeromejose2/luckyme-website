

<div class="popup-wrapper">
    
    <a href="javascript:void(0);" class="btn-close" onclick="lytebox.close();">&times;</a>

    <div class="popup-outer">
        
        <div class="popup-inner">
                
            <!-- append like inner0recipe0section -->
                <div class="recipe-preview">
                    
                    <!-- <h2>Featured Recipe</h2> -->
                    
                    <div class="img-wrapper margintop">
                        
                        <img class="featured-img lazyload" data-src="{{ $data->photo }}">
                    
                    </div>
                    
                    <img class="swakpack lazyload" data-src="{{ URL::asset($product->namnam_product_shot) }}">
                    
                    <p class="title">{{ $data->recipe_name }} <br> <span style="color:black; font-size: small;"> Dish by : {{ $data->dish_by }} </span></p>
                    
                    <div class="heart-vote">

                        <div id="inner-ratings" class="ratings" data-ratings="{{ $average }}" data-id="{{ $data->id }}">
                            <button><span></span></button>
                            <button><span></span></button>
                            <button><span></span></button>
                            <button><span></span></button>
                            <button><span></span></button>
                        </div>

			<span class="lbl-votes"><span id="inner-total-votes">{{ $total_votes }}</span> votes</span>

                    </div>

                    <p class="label"><span class="icons-recipe-time"></span>Preparation Time: <span class="data-time data">{{ $data->prep_time }}</span></p>
                    
                    <p class="label"><span class="icons-recipe-people"></span>Serving Size: <span class="data-serve data">{{ $data->serving_size }}</span></p>

                </div>

                <div class="recipe-steps">

                    <div id="recipe-inner-content-div">
                        @if($data != 'none')
                            <div class="custom-font">
                            	<p>
                                	{{ $data->description }}
                                </p>
                            </div>
                        @endif
                        <p>
                            <b class="recipe-header"> What You Need </b> <br>
                            @foreach($ingredients as $i)
                                 {{ nl2br($i->qty.' '.$i->name) }} <br>
                            @endforeach

                        </p>
                        <p>
                            <b class="recipe-header"> How To Cook </b> <br>
                            @foreach($procedures as $p)
                                {{nl2br($p->name)}} <br>
                                {{nl2br($p->item)}} <br> <br>
                            @endforeach
                        </p>

                    </div>

                </div>

                <div class="side-footer">
                    <div id="popupshare" class="cf">

                        <div class="facebook" data-url="{{ URL::to('/?recipe_id='.$data->id) }}" data-text="Check out this {{ $data->recipe_name }} recipe with NamNam!"></div>
                        <div class="twitter" data-url="{{ Setting::bitly(URL::to('/?recipe_id='.$data->id)) }}" data-text="Check out this {{ $data->recipe_name }} recipe with NamNam!"></div>
                    
                        <!--new button-->
                        <a target="_blank" href="{{ URL::to('recipe/print/preview', array('id' => $data->id)) }}" class="btn blue-print">Print</a>
                        <a href="{{ URL::to('recipe/print', array('id' => $data->id)) }}" class="btn blue-download">Download</a>

                    </div>

                    <div>
                        <a href="" class="header-info"> Have a recipe to share?<br/> <em>Email us at luckymenamnam@mondenissin.com.tph<br/> or Snail mail it to us at 9 Sheridan St., Guerrero Compound,<br/> Brgy. Buayang Bato, Mandaluyong City. </em></a>
                    </div>

                    
                </div>
                <div class="clearfix"></div>
        </div>

    </div>

</div>

<span data-via="{{ $via }}" id="via_tw"> </span>
<script type="text/javascript">
    var tw_via = $("#via_tw").data('via');
    $('.twitter').sharrre({
      share: {
        twitter: true
      },
      template: '<a href="#"><div class="share"><span></span>Tweet</div></a>',
      enableHover: false,
      enableTracking: true,
      buttons: { twitter: {via: tw_via}},
      click: function(api, options){
        api.simulateClick();
        api.openPopup('twitter');
      }
    });
    $('.facebook').sharrre({
      share: {
        facebook: true
      },
      template: '<a href="#"><div class="share"><span></span>Share</div></a>',
      enableHover: false,
      enableTracking: true,
      click: function(api, options){
        api.simulateClick();
        api.openPopup('facebook');
      }
    });

    $(function(){
        $('#recipe-inner-content-div').slimScroll({
            position: 'right',
            height: '545px',
            railVisible: true,
            alwaysVisible: true
        });
    });

    function innerRatings(){
        $('#inner-ratings button').removeClass('active');
        $('#inner-ratings button span').css({width:'100%'});
        
        $('#inner-ratings button').each(function(){
            var me = $(this);
            var index = me.index('#inner-ratings button') + 1;
            var ratings = me.parent().data('ratings');

            if( index <= Math.ceil(ratings) ){
                me.addClass('active');
            }

            if( index == Math.ceil(ratings) ){
                var decimal =  Math.round(100 - ((index - ratings) * 100)) ;
                me.children().css({width: decimal + '%'});
            }
        });
    }

    $(function(){
        innerRatings();
        $('#inner-ratings button').click(function(){
            var me = $(this);
            var index = me.index('#inner-ratings button') + 1;
            $('#inner-ratings').data('ratings',index);
            var id = $('#inner-ratings').data('id');
            ajaxInnerRate(id, index);
            innerRatings();
        });
    });


    function ajaxInnerRate(r_recipe, r_rating)
    {

        $.ajax({
             type: "POST",
             url: "/ajax/reciperate",
             dataType: "json",
             data: { recipe: r_recipe, rating: r_rating },
             success : function(data) {
                if(data['msg'] == 'S')
                {
                    alert('You have successfully rated this recipe!');
                    $('#inner-ratings').data('ratings', data['average']), innerRatings();
                }
                else if(data['msg'] == 'ERR1')
                {
                    alert('You have already rated this recipe!');
                    $('#inner-ratings').data('ratings', data['average']), innerRatings();
                }
				$('#inner-total-votes').html(data['total']);
             },
             complete: function(){
                //Add action here for when the ajax process is finished
             }
        });
    }

</script>