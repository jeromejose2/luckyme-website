<?php

class HomepageSliderController extends BaseController {

    /**
     * Display a listing of homepagesliders
     *
     * @return Response
     */
    public function index()
    {
        $homepagesliders = HomepageSlider::all();

        return View::make('cms.homepage_sliders.index')->with('homepagesliders', $homepagesliders);
    }

    /**
     * Show the form for creating a new homepageslider
     *
     * @return Response
     */
    public function create()
    {
        return View::make('cms.homepage_sliders.create');
    }

    /**
     * Store a newly created homepageslider in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make($data = Input::all(), HomepageSlider::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $v_code = HomepageSlider::getYouTubeID($data['bg_video_url']);

        $filename_photo = '';
        $destinationPath_photo = '';

        $mobile_filename_photo = '';
        $mobile_destinationPath_photo = '';
        if (Input::hasFile('slider-photo'))
        {
            $file_photo = Input::file('slider-photo');
            $destinationPath_photo = '/assets/uploads/photo/';
            $extension_photo = $file_photo->getClientOriginalExtension();
            $filename_photo = str_random(11).".{$extension_photo}";
            $upload_success_photo = $file_photo->move(public_path() . $destinationPath_photo, $filename_photo);
        }

        if (Input::hasFile('mobile-photo'))
        {
            $mobile_file_photo = Input::file('mobile-photo');
            $mobile_destinationPath_photo = '/assets/uploads/photo/';
            $extension_photo = $file_photo->getClientOriginalExtension();
            $mobile_filename_photo = "mobile_".str_random(11).".{$extension_photo}";
            $upload_success_photo = $mobile_file_photo->move(public_path() . $mobile_destinationPath_photo, $mobile_filename_photo);
        }

        if (Input::has('bg_video_url') && $data['slider-photo'] === null)
        {
            $homepageslider = new HomepageSlider;
            $homepageslider->slider_id             = $data['slider_id'];
            $homepageslider->photo                 = "";
            $homepageslider->button1_url_copyline1 = "";
            $homepageslider->button1_url_copyline2 = "";
            $homepageslider->button1_fontcolor     = "";
            $homepageslider->button1_bgcolor       = "";
            $homepageslider->button1_url_bg        = "";
            $homepageslider->button2_url_copyline1 = "";
            $homepageslider->button2_url_copyline2 = "";
            $homepageslider->button2_fontcolor     = "";
            $homepageslider->button2_bgcolor       = "";
            $homepageslider->button2_url_bg        = "";
            $homepageslider->bg_video_url          = $v_code;
            $homepageslider->content_type          = 'Video';
            $homepageslider->save();
        }
        elseif ($data['bg_video_url'] === '')
        {
            $homepageslider = new HomepageSlider;
            $homepageslider->slider_id             = $data['slider_id'];
            $homepageslider->photo                 = $destinationPath_photo.$filename_photo;
            $homepageslider->mobile_photo          = $mobile_destinationPath_photo.$mobile_filename_photo;
            $homepageslider->button1_url_copyline1 = $data['button1_url_copyline1'];
            $homepageslider->button1_url_copyline2 = $data['button1_url_copyline2'];
            $homepageslider->button1_url_bg        = $data['button1_url_bg'];
            $homepageslider->button1_fontcolor     = Input::get('button1_fontcolor', '');
            $homepageslider->button1_bgcolor       = Input::get('button1_bgcolor', '');
            $homepageslider->button2_url_copyline1 = $data['button2_url_copyline1'];
            $homepageslider->button2_url_copyline2 = $data['button2_url_copyline2'];
            $homepageslider->button2_url_bg        = $data['button2_url_bg'];
            $homepageslider->button2_fontcolor     = Input::get('button2_fontcolor', '');
            $homepageslider->button2_bgcolor       = Input::get('button2_bgcolor', '');
            $homepageslider->bg_video_url          = "";
            $homepageslider->content_type          = 'Image';
            $homepageslider->save();
        }

        return Redirect::route('manage.homepage-sliders.index');
    }

    /**
     * Display the specified homepageslider.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $homepageslider = HomepageSlider::findOrFail($id);

        return View::make('cms.homepage_sliders.show')->with('homepageslider', $homepageslider);
    }

    /**
     * Show the form for editing the specified homepageslider.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $homepageslider = HomepageSlider::find($id);

        return View::make('cms.homepage_sliders.edit')->with('homepageslider', $homepageslider);
    }

    /**
     * Update the specified homepageslider in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validator = Validator::make($data = Input::all(), HomepageSlider::$updateRules);

        $homepageslider = HomepageSlider::findOrFail($id);

        // check if slider_id exist or not the same as current slider_id in DB
        if (HomepageSlider::where('slider_id', '=', $data['slider_id'])->exists() &&
            $data['slider_id'] != $homepageslider->slider_id)
        {
            $updateError = new Illuminate\Support\MessageBag;
            $updateError->add('exist', 'The <b>Slider ID</b> is currently in use, please choose another one ');

            return Redirect::back()->withErrors($updateError->all())->withInput();
        }

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        // update for video
        if ($homepageslider->content_type === 'Video')
        {
            $v_code = HomepageSlider::getYouTubeID($data['bg_video_url']);
            $data['bg_video_url'] = $v_code;

            $homepageslider->bg_video_url = $data['bg_video_url'];
            $homepageslider->save();
        }
        elseif ($homepageslider->content_type === 'Image')
        {
            $homepageslider->update($data);
            $photo = HomepageSlider::find($id);
            $photo->button1_url_bg = $data['button1_url_bg'];
            $photo->button2_url_bg = $data['button2_url_bg'];
            $photo->button1_fontcolor     = Input::get('button1_fontcolor', '');
            $photo->button1_bgcolor       = Input::get('button1_bgcolor', '');
            $photo->button2_fontcolor     = Input::get('button2_fontcolor', '');
            $photo->button2_bgcolor       = Input::get('button2_bgcolor', '');
            $photo->bg_video_url   = '';
            $photo->save();

            if (Input::hasFile('slider-photo'))
            {
                $file_photo = Input::file('slider-photo');
                $destinationPath_photo = '/assets/uploads/photo/';
                $extension_photo = $file_photo->getClientOriginalExtension();
                $filename_photo = str_random(11).".{$extension_photo}";
                $upload_success_photo = $file_photo->move(public_path() . $destinationPath_photo, $filename_photo);

                $photo = HomepageSlider::find($id);
                $photo->photo = $destinationPath_photo.$filename_photo;
                $photo->save();
            }
            if (Input::hasFile('mobile-photo'))
            {
                $mobile_file_photo = Input::file('mobile-photo');
                $mobile_destinationPath_photo = '/assets/uploads/photo/';
                $mobile_extension_photo = $mobile_file_photo->getClientOriginalExtension();
                $mobile_filename_photo = "mobile_".str_random(11).".{$mobile_extension_photo}";
                $upload_success_photo = $mobile_file_photo->move(public_path() . $mobile_destinationPath_photo, $mobile_filename_photo);
                
                $photo = HomepageSlider::find($id);
                $photo->mobile_photo = $mobile_destinationPath_photo.$mobile_filename_photo;
                $photo->save();
            }
        }


        return Redirect::route('manage.homepage-sliders.index');
    }

    /**
     * Remove the specified homepageslider from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        HomepageSlider::destroy($id);

        return Redirect::route('manage.homepage-sliders.index');
    }

    /**
     * Fetch HomepageSlider columns using AJAX
     *
     */
    public function getDatatable()
    {
        return Datatable::collection(HomepageSlider::all())
            ->showColumns('slider_id', 'content_type')
            ->addColumn('content_type', function($model) {
                if($model->content_type === 'Image')
                {
                    $ahref = '<a href='.URL::asset($model->photo).' class="highslide" onclick="return hs.expand(this)">';
                    $img = HTML::image($model->photo,"namnam", array('style' => 'width: 100px; height: 100px;'));

                    return $ahref.$img.'</a>';
                }
                elseif($model->content_type === 'Video')
                {
                    $vid = '<iframe width="330" height="208" src="//www.youtube.com/embed/'.$model->bg_video_url.'?rel=0" frameborder="0" allowfullscreen></iframe>';

                    return $vid;
                }
            })
            ->addColumn('view', function($model) {

                return HTML::linkRoute('manage.homepage-sliders.show', 'View', $model->id, array('class' => 'btn btn-default btn-block'));
            })
            ->addColumn('edit', function($model) {

                if (Auth::user()->isVisitor())
                {
                    return '';
                }
                else
                {
                    return HTML::linkRoute('manage.homepage-sliders.edit', 'Edit', $model->id, array('class' => 'btn btn-default btn-block'));
                }

            })
            ->addColumn('delete', function($model) {
                $form_open   = Form::open(array('route' => array('manage.homepage-sliders.destroy', $model->id), 'method' => 'delete'));
                $form_button = Form::submit('Delete', array('class' => 'btn btn-danger'));
                $form_close  = Form::close();
                $button = '<!-- Button trigger modal -->
                <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#myModal'.$model->id.'">
                  Delete
                </button>';
                $modal = '<!-- Modal -->
                <div class="modal fade" id="myModal'.$model->id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Delete confirmation</h4>
                      </div>
                      <div class="modal-body">
                        Are you sure you want to delete?
                      </div>
                      <div class="modal-footer">
                        '.$form_open.$form_button.$form_close.'
                      </div>
                    </div>
                  </div>
                </div>';
                return $modal.$button;
            })
            ->searchColumns('slider_id', 'content_type')
            ->orderColumns('slider_id', 'content_type')
            ->make();
    }

}
