<?php

class BadgesController extends \BaseController {

	/**
	 * Display a listing of badges
	 *
	 * @return Response
	 */
	public function index()
	{
		$badges = Badge::all();

		return View::make('badges.index', compact('badges'));
	}

	/**
	 * Show the form for creating a new badge
	 *
	 * @return Response
	 */
	public function create()
	{
		$recipe = Recipe::lists('recipe_name', 'id');
		return View::make('badges.create')
		->with('recipes', $recipe);
	}

	/**
	 * Store a newly created badge in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Badge::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$data = Input::all();
        $new_badges = new Badge;
        $new_badges->recipe_id = $data['recipe'];
        $new_badges->badge_choosen = $data['approval'];
        $new_badges->save();

		return Redirect::route('manage.badges.index');
	}

	/**
	 * Display the specified badge.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$badge = Badge::findOrFail($id);

		return View::make('badges.show', compact('badge'));
	}

	/**
	 * Show the form for editing the specified badge.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$badge = Badge::find($id);

		return View::make('badges.edit', compact('badge'));
	}

	/**
	 * Update the specified badge in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$badge = Badge::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Badge::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$badge->update($data);

		return Redirect::route('badges.index');
	}

	/**
	 * Remove the specified badge from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Badge::destroy($id);

		return Redirect::route('badges.index');
	}

}
