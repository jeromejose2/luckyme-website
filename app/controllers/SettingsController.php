<?php

class SettingsController extends BaseController {

	const DESTINATION_PATH_BGMUSIC = 'assets/';
    /**
     * Display a listing of settings
     *
     * @return Response
     */
    public function index()
    {
        $settings = Setting::orderBy('type', 'ASC')->get();

        return View::make('cms.settings.index')->with('settings', $settings);
    }

    /**
     * Show the form for creating a new setting
     *
     * @return Response
     */
    public function create()
    {
        return View::make('cms.settings.create');
    }

    /**
     * Store a newly created setting in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make($data = Input::all(), Setting::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Setting::create($data);

        return Redirect::route('manage.settings.index');
    }

    /**
     * Display the specified setting.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $setting = Setting::findOrFail($id);

        return View::make('cms.settings.show')->with('setting', $setting);
    }

    /**
     * Show the form for editing the specified setting.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $setting = Setting::find($id);

        return View::make('cms.settings.edit')->with('setting', $setting);
    }

    /**
     * Update the specified setting in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $setting = Setting::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Setting::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $setting = Setting::find($id);
        $setting->type    = $data['type'];
        $setting->content = $data['content'];
        $setting->save();

        if (Input::hasFile('new-bgm'))
        {
            $file_music = Input::file('new-bgm');
			$destinationPath_music = self::DESTINATION_PATH_BGMUSIC;
            $extension_music = $file_music->getClientOriginalExtension();
            $filename_music = str_random(11).".{$extension_music}";
            $upload_success_music = $file_music->move(public_path() . '/' . $destinationPath_music, $filename_music);
            $setting->content = $destinationPath_music.$filename_music;
            $setting->update();
        }

        return Redirect::route('manage.settings.index');
    }

    /**
     * Remove the specified setting from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return Redirect::route('manage.settings.index');
    }

    /**
     * Fetch Settings columns using AJAX
     *
     */
    public function getDatatable()
    {
        return Datatable::collection(Setting::all())
            ->showColumns('type', 'content')
            ->addColumn('view', function($model) {

                return HTML::linkRoute('manage.settings.show', 'View', $model->id, array('class' => 'btn btn-default btn-block'));
            })
            ->addColumn('edit', function($model) {

                if (Auth::user()->isVisitor())
                {
                    return '';
                }
                else
                {
                    return HTML::linkRoute('manage.settings.edit', 'Edit', $model->id, array('class' => 'btn btn-default btn-block'));
                }

            })
            ->searchColumns('type', 'content')
            ->orderColumns('type', 'content')
            ->make();
    }

}
