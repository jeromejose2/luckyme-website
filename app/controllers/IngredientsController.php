<?php

class IngredientsController extends \BaseController {

	/**
	 * Display a listing of ingredients
	 *
	 * @return Response
	 */
	public function index()
	{
		$ingredients = Ingredient::all();

		return View::make('ingredients.index', compact('ingredients'));
	}

	/**
	 * Show the form for creating a new ingredient
	 *
	 * @return Response
	 */
	public function create()
	{
		$recipe = Recipe::lists('recipe_name', 'id');
		return View::make('ingredients.create')
		->with('recipes', $recipe);
	}

	/**
	 * Store a newly created ingredient in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Ingredient::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$data = Input::all();
        $new_badges = new Ingredient;
        $new_badges->recipe_id = $data['recipe'];
        $new_badges->name = $data['name'];
        $new_badges->qty = $data['qty'];
        $new_badges->save();

		return Redirect::route('manage.ingredients.index');
	}

	/**
	 * Display the specified ingredient.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$ingredient = Ingredient::findOrFail($id);

		return View::make('ingredients.show', compact('ingredient'));
	}

	/**
	 * Show the form for editing the specified ingredient.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $ingredient = Ingredient::find($id);
        $the_recipe = Recipe::find($ingredient->recipe_id);
        if(Session::has('recipe_id'))
        {
            $data = Session::all();
            return View::make('ingredients.edit', compact('ingredient'))
            ->with('recipe', $the_recipe)
            ->with('ingredients_mode', 'true')
            ->with('recipe_id', $data['recipe_id']);
        }
		return View::make('ingredients.edit', compact('ingredient'))
		->with('recipe', $the_recipe);
	}

	/**
	 * Update the specified ingredient in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$ingredient = Ingredient::findOrFail($id);
        $data = Input::all();
        if(Input::has('recipe_id'))
        {
            $recipe_id = $data['recipe_id'];
            unset($data['recipe_id']);
        }
		$validator = Validator::make($data, Ingredient::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$ingredient->update($data);

        if(isset($recipe_id))
        {
            return Redirect::route('manage.recipes.create')
                        ->with('ingredients_mode', 'true')
                        ->with('recipe_id', $recipe_id);
        }

		return Redirect::route('manage.ingredients.index');
	}

	/**
	 * Remove the specified ingredient from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Ingredient::destroy($id);

		return Redirect::route('ingredients.index');
	}

}
