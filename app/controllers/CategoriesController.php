<?php

class CategoriesController extends \BaseController {

	/**
	 * Display a listing of categories
	 *
	 * @return Response
	 */
	public function index()
	{
		$categories = Category::all();

		return View::make('categories.index', compact('categories'));
	}

	/**
	 * Show the form for creating a new category
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('categories.create');
	}

	/**
	 * Store a newly created category in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Category::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Category::create($data);

		return Redirect::route('manage.categories.index');
	}

	/**
	 * Display the specified category.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$category = Category::findOrFail($id);

		return View::make('categories.show', compact('category'));
	}

	/**
	 * Show the form for editing the specified category.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = Category::find($id);

		return View::make('categories.edit', compact('category'));
	}

	/**
	 * Update the specified category in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$category = Category::findOrFail($id);
        $oldcat = $category->name;
		$validator = Validator::make($data = Input::all(), Category::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$category->update($data);
        $cat = $data['name'];
        DB::table('recipes')->where('category', $oldcat)->update(array('category' => $cat));

		return Redirect::route('manage.categories.index');
	}

	/**
	 * Remove the specified category from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $c = Category::find($id);
        $oldcat = $c->name;
		Category::destroy($id);
        DB::table('recipes')->where('category', $oldcat)->update(array('category' => 'None'));
		return Redirect::route('manage.categories.index');
	}


    /**
     * Fetch Category columns using AJAX
     *
     */
    public function getDatatable()
    {
        return Datatable::collection(Category::all())
            ->showColumns('id', 'name')
            ->addColumn('edit', function($model) {
                if($model->name == 'All')
                {
                    return HTML::linkRoute('manage.categories.edit', 'Edit Category', $model->id, array('class' => 'btn btn-default btn-block disabled'));
                }
                else
                {
                    return HTML::linkRoute('manage.categories.edit', 'Edit Category', $model->id, array('class' => 'btn btn-default btn-block'));
                }
            })
			->addColumn('delete', function($model) {
                if (Auth::user()->isVisitor())
                {
                    return '';
                }
                else
                {
					if($model->name !== 'All') {
                    $form_open   = Form::open(array('route' => array('manage.categories.destroy', $model->id), 'method' => 'delete'));
                    $form_button = Form::submit('Delete', array('class' => 'btn btn-danger'));
                    $form_close  = Form::close();

                    $button = '<!-- Button trigger modal -->
                    <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#myModal'.$model->id.'">
                      Delete
                    </button>';
                    $modal = '<!-- Modal -->
                    <div class="modal fade" id="myModal'.$model->id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Delete confirmation</h4>
                          </div>
                          <div class="modal-body">
                            Are you sure you want to delete?
                          </div>
                          <div class="modal-footer">
                            '.$form_open.$form_button.$form_close.'
                          </div>
                        </div>
                      </div>
                    </div>';
                    return $modal.$button;
					}
                }
            })
            ->searchColumns('id', 'name')
            ->orderColumns('id', 'name')
            ->make();
    }


}
