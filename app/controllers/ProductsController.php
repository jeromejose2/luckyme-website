<?php

class ProductsController extends BaseController {

    /**
     * Display a listing of products
     *
     * @return Response
     */
    public function index()
    {

        $products = Product::all();

        return View::make('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new product
     *
     * @return Response
     */
    public function create()
    {

        return View::make('products.create');

    }

    /**
     * Store a newly created product in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make($data = Input::all(), Product::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $destinationPath = '';
        $filename = '';
            if (Input::hasFile('Image-NamNam'))
            {
                $file = Input::file('Image-NamNam');
                $destinationPath = '/assets/uploads/namnamproduct/';
                $extension = $file->getClientOriginalExtension();
                $filename = str_random(12).".{$extension}";
                $upload_success_namnam = $file->move(public_path() . $destinationPath, $filename);
            }


            $data = Input::all();
            $new_product = new Product;
            $new_product->title = $data['title'];
            $new_product->description = $data['description'];
            $new_product->namnam_product_shot = $destinationPath.$filename;
            $new_product->url = $data['url'];
            $new_product->save();

        return Redirect::route('manage.products.index');
    }

    /**
     * Display the specified product.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $recipes =  Recipe::where('product_id',$id)->get();
        return View::make('products.show', compact('product'))
        ->with('recipes', $recipes);
    }

    /**
     * Show the form for editing the specified product.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return View::make('products.edit', compact('product'));
    }

    /**
     * Update the specified product in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $product = Product::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Product::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $product->update($data);


        if (Input::hasFile('Image-NamNam'))
        {
            $file = Input::file('Image-NamNam');
            $destinationPath = '/assets/uploads/namnamproduct/';
            $extension = $file->getClientOriginalExtension();
            $filename = str_random(12).".{$extension}";
            $upload_success_namnam = $file->move(public_path() . $destinationPath, $filename);

        $photo = Product::find($id);
        $photo->namnam_product_shot = $destinationPath.$filename;
        $photo->save();
        File::delete($product->namnam_product_shot);
        }
        else
        {

        }


        return Redirect::route('manage.products.index');
    }

    /**
     * Remove the specified product from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Product::destroy($id);

        return Redirect::route('products.index');
    }

    /**
     * Fetch Product columns using AJAX
     *
     */
    public function getDatatable()
    {
        return Datatable::collection(Product::all())
            ->showColumns('title', 'description', 'url', 'namnam_product_shot')
            ->addcolumn('description', function($model) {
                return str_limit($model->description,120,'...');
            })
            ->addColumn('namnam_product_shot', function($model) {
                $ahref = '<a href='.URL::asset($model->namnam_product_shot).' class="highslide" onclick="return hs.expand(this)">';
                $img = HTML::image($model->namnam_product_shot,"namnam", array('style' => 'width: 100px; height: 100px;'));

                return $ahref.$img.'</a>';
            })
            ->addColumn('view', function($model) {
                return HTML::linkRoute('manage.products.show', 'View', $model->id, array('class' => 'btn btn-default btn-block'));
            })
            ->addColumn('edit', function($model) {
                return HTML::linkRoute('manage.products.edit', 'Edit', $model->id, array('class' => 'btn btn-default btn-block'));
            })
            ->searchColumns('title', 'description', 'url', 'namnam_product_shot')
            ->orderColumns('title', 'description', 'url', 'namnam_product_shot')
            ->make();
    }

}
