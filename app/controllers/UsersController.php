<?php

class UsersController extends BaseController {

    /**
     * Display a listing of users
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();

        return View::make('cms.users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new user
     *
     * @return Response
     */
    public function create()
    {
        return View::make('cms.users.create');
    }

    /**
     * Store a newly created user in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make($data = Input::all(), User::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        User::create([
            'email'        => $data['email'],
            'password'     => $data['password'],
            'name'         => $data['name'],
            'username'     => $data['username'],
            'access_level' => $data['access_level']
        ]);

        return Redirect::route('manage.users.index');
    }

    /**
     * Display the specified user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return View::make('cms.users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return View::make('cms.users.edit')->with('user', $user);
    }

    /**
     * Update the specified user in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validator = Validator::make($data = Input::all(), User::$updateRules);
        $user = User::find($id);

        // check if email exist or not the same as current email in DB
        if (User::where('email', '=', $data['email'])->exists() &&
            $data['email'] != $user->email)
        {
            $updateError = new Illuminate\Support\MessageBag;
            $updateError->add('exist', 'The <b>E-mail</b> is already used, please choose another one ');

            return Redirect::back()->withErrors($updateError->all())->withInput();
        }

        // check if slider_id exist or not the same as current slider_id in DB
        if (User::where('username', '=', $data['username'])->exists() &&
            $data['username'] != $user->username)
        {
            $updateError = new Illuminate\Support\MessageBag;
            $updateError->add('exist', 'The <b>username</b> is already used, please choose another one ');

            return Redirect::back()->withErrors($updateError->all())->withInput();
        }

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $user->email        = $data['email'];
        $user->password     = $data['password'];
        $user->name         = $data['name'];
        $user->username     = $data['username'];
        $user->access_level = $data['access_level'];
        $user->save();

        return Redirect::route('manage.users.index');
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return Redirect::route('manage.users.index');
    }

    /**
     * Shows the User profile (self)
     *
     */
    public function showProfile()
    {
        $user = Auth::user();

        return View::make('cms.users.profile')
            ->with('user', $user);
    }

    /**
     * Edits the User profile (self)
     *
     */
    public function editProfile()
    {
        $user = Auth::user();

        return View::make('cms.users.profile-edit')
            ->with('user', $user);
    }

    /**
     * Fetch User columns using AJAX
     *
     */
    public function getDatatable()
    {
        return Datatable::collection(User::all())
            ->showColumns('username','email', 'name')
            ->addColumn('view', function($model) {
               return HTML::linkRoute('manage.users.show', 'View', $model->id, array('class' => 'btn btn-default btn-block'));
            })
            ->addColumn('edit', function($model) {
               return HTML::linkRoute('manage.users.edit', 'Edit', $model->id, array('class' => 'btn btn-default btn-block'));
            })
            ->addColumn('delete', function($model) {
                $form_open   = Form::open(array('route' => array('manage.users.destroy', $model->id), 'method' => 'delete'));
                $form_button = Form::submit('Delete', array('class' => 'btn btn-danger'));
                $form_close  = Form::close();

                $button = '<!-- Button trigger modal -->
                <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#myModal'.$model->id.'">
                  Delete
                </button>';
                $modal = '<!-- Modal -->
                <div class="modal fade" id="myModal'.$model->id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Delete confirmation</h4>
                      </div>
                      <div class="modal-body">
                        Are you sure you want to delete?
                      </div>
                      <div class="modal-footer">
                        '.$form_open.$form_button.$form_close.'
                      </div>
                    </div>
                  </div>
                </div>';
                return $modal.$button;
            })
            ->searchColumns('username','email', 'name')
            ->orderColumns('username','email', 'name')
            ->make();
    }

}
