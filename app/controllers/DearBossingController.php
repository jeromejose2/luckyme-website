<?php

class DearBossingController extends BaseController {

    /**
     * Display a listing of dearbossings
     *
     * @return Response
     */
    public function index()
    {
        $dearbossings = DearBossing::all();
        $count_active = DearBossing::where('is_active', 1)->count();
        $count_inactive = count($dearbossings) - $count_active;
        return View::make('cms.dear_bossings.index')->with('dearbossings', $dearbossings)
                    ->with('active', $count_active)
                    ->with('inactive', $count_inactive);
    }

    /**
     * Show the form for creating a new dearbossing
     *
     * @return Response
     */
    public function create()
    {
        return View::make('cms.dear_bossings.create');
    }

    /**
     * Store a newly created dearbossing in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make($data = Input::all(), DearBossing::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        DearBossing::create($data);

        return Redirect::route('manage.dear-bossing.index');
    }

    /**
     * Display the specified dearbossing.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $dearbossing = DearBossing::findOrFail($id);

        return View::make('cms.dear_bossings.show')->with('dearbossing', $dearbossing);
    }

    /**
     * Show the form for editing the specified dearbossing.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $dearbossing = DearBossing::find($id);

        return View::make('cms.dear_bossings.edit')->with('dearbossing', $dearbossing);
    }

    /**
     * Update the specified dearbossing in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validator   = Validator::make($data = Input::all(), DearBossing::$updateRules);
        $dearbossing = DearBossing::findOrFail($id);

        // check if episode_id exist or not the same as current episode_id in DB
        // if (DearBossing::where('episode_id', '=', $data['episode_id'])->exists() &&
        //     $data['episode_id'] != $dearbossing->episode_id)
        // {
        //     $updateError = new Illuminate\Support\MessageBag;
        //     $updateError->add('exist', 'The <b>Episode ID</b> is currently in use, please choose another one ');

        //     return Redirect::back()->withErrors($updateError->all())->withInput();
        // }

        // check if title exist or not the same as current title in DB
        if(DearBossing::where('title', '=', $data['title'])->exists() &&
            $data['title'] != $dearbossing->title)
        {
            $updateError = new Illuminate\Support\MessageBag;
            $updateError->add('exist', 'The <b>Title</b> is currently in use, please choose another one ');

            return Redirect::back()->withErrors($updateError->all())->withInput();
        }

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $dearbossing->episode_id    = $data['episode_id'];
        $dearbossing->title         = $data['title'];
        $dearbossing->article       = $data['article'];
        $dearbossing->youtube_url   = $data['youtube_url'];
        $dearbossing->email_address = $data['email_address'];
        $dearbossing->is_active     = $data['is_active'];
        $dearbossing->save();

        return Redirect::route('manage.dear-bossing.index');
    }

    /**
     * Remove the specified dearbossing from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        DearBossing::destroy($id);

        return Redirect::route('manage.dear-bossing.index');
    }

    /**
     * Fetch User columns using AJAX
     *
     */
    public function getDatatable()
    {
        return Datatable::collection(DearBossing::all())
            ->showColumns('title', 'article', 'youtube_url', 'active', 'email_address')
            ->addColumn('article', function($model) {
                return str_limit($model->article, $limit = 30, $end = '...') ;
            })
            ->addColumn('youtube_url', function($model) {
                return '<a href="{{ URL::to($model->youtube_url) }}" target="_blank">Click Here</a>';
            })
            ->addColumn('active', function($model) {

                if ($model->is_active)
                {
                    $active = '<i class="fa fa-check-circle fa-fw fa-2x text-success"></i>';
                }
                else
                {
                    $active = '<i class="fa fa-times-circle fa-fw fa-2x text-danger"></i>';
                }

                return $active;
            })
            ->addColumn('view', function($model) {

                return HTML::linkRoute('manage.dear-bossing.show', 'View', $model->id, array('class' => 'btn btn-default btn-block'));
            })
            ->addColumn('edit', function($model) {

                if (Auth::user()->isVisitor())
                {
                    return '';
                }
                else
                {
                    return HTML::linkRoute('manage.dear-bossing.edit', 'Edit', $model->id, array('class' => 'btn btn-default btn-block'));
                }

            })
            ->addColumn('delete', function($model) {

                if (Auth::user()->isVisitor())
                {
                    return '';
                }
                else
                {
                    $form_open   = Form::open(array('route' => array('manage.dear-bossing.destroy', $model->id), 'method' => 'delete'));
                    $form_button = Form::submit('Delete', array('class' => 'btn btn-danger'));
                    $form_close  = Form::close();

                    $button = '<!-- Button trigger modal -->
                    <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#myModal'.$model->id.'">
                      Delete
                    </button>';
                    $modal = '<!-- Modal -->
                    <div class="modal fade" id="myModal'.$model->id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Delete confirmation</h4>
                          </div>
                          <div class="modal-body">
                            Are you sure you want to delete?
                          </div>
                          <div class="modal-footer">
                            '.$form_open.$form_button.$form_close.'
                          </div>
                        </div>
                      </div>
                    </div>';
                    return $modal.$button;
                }
            })
            ->searchColumns('title', 'article', 'youtube_url', 'active', 'email_address')
            ->orderColumns('title', 'article', 'youtube_url', 'active', 'email_address')
            ->make();
    }

}
