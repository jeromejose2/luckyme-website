<?php

class RecipesController extends BaseController {

	const DESTINATION_PATH_PHOTO = 'assets/uploads/photo/';

    /**
     * Display a listing of recipes
     *
     * @return Response
     */
    public function index()
    {
        $recipes = Recipe::all();
        $original_count = Recipe::where('product_id', 1)->count();
        $tomato_count = Recipe::where('product_id', 2)->count();
        return View::make('recipes.index', compact('recipes'))
                    ->with('original_count', $original_count)
                    ->with('tomato_count', $tomato_count)
                    ->with('count', count($recipes));
    }

    /**
     * Show the form for creating a new recipe
     *
     * @return Response
     */
    public function create()
    {
        if(Session::get('ingredients_mode', 'false') == 'true')
        {
           $recipe = Session::get('recipe_id', 0);
           $recipe = Recipe::find($recipe);
           $ingredients = Ingredient::where('recipe_id', $recipe->id)->get();
           $procedures = Procedure::where('recipe_id', $recipe->id)->get();
           $categories = Category::where('name', '!=' ,'All')->lists('name', 'name');
           return View::make('recipes.createIngredients')
                   ->with('categories', $categories)
                   ->with('ingredients', $ingredients)
                   ->with('procedures', $procedures)
                   ->with('recipe', $recipe);
        }

        $products = Product::lists('title', 'id');
        $categories = Category::where('name', '!=' ,'All')->lists('name', 'name');
        return View::make('recipes.create')
                   ->with('categories', $categories)
        ->with('products', $products);
    }

    /**
     * Store a newly created recipe in storage.
     *
     * @return Response
     */
    public function store()
    {
        if(Input::has('edit_btn'))
        {
            $data = Input::all();
            if(Input::get('ingredients_mode', 'false') == 'true')
            {
                // Session::flash('ingredients_mode', 'true');
                // Session::flash('recipe_id', $data['recipe_id']);
                // REDIRECT::ROUTE + WITH IS SESSION FLASH
                return Redirect::route('manage.ingredients.edit', array('id' => Input::get('id')))
                        ->with('ingredients_mode', 'true')
                        ->with('recipe_id', $data['recipe_id']);
            }
            elseif(Input::get('procedures_mode', 'false') == 'true')
            {
                // Session::flash('procedures_mode', 'true');
                // Session::flash('recipe_id', $data['recipe_id']);
                return Redirect::route('manage.procedures.edit', array('id' => Input::get('id')))
                        ->with('procedures_mode', 'true')
                        ->with('recipe_id', $data['recipe_id']);
            }
        }
        if(Input::get('ingredients_mode', 'false') == 'true')
        {
            if(Input::get('delete', 'false') == 'true')
            {
                $data = Input::all();
                Ingredient::where('recipe_id', $data['recipe_id'])
                            ->where('id', $data['id'])
                            ->delete();
            }
            else
            {
                $data = Input::all();
                $ingredient = new Ingredient;
                $ingredient->recipe_id = $data['recipe_id'];
                $ingredient->name      = $data['name'];
                $ingredient->qty       = $data['qty'];
                $ingredient->save();
            }
            Session::flash('ingredients_mode', 'true');
            Session::flash('recipe_id', $data['recipe_id']);

            return Redirect::route('manage.recipes.create');
        }
        elseif(Input::get('procedures_mode', 'false') == 'true')
        {
            if(Input::get('delete', 'false') == 'true')
            {
                $data = Input::all();
                Procedure::where('recipe_id', $data['recipe_id'])
                            ->where('id', $data['id'])
                            ->delete();
            }
            else
            {
                $data = Input::all();
                $ingredient = new Procedure;
                $ingredient->recipe_id = $data['recipe_id'];
                $ingredient->name      = $data['name'];
                $ingredient->item       = $data['item'];
                $ingredient->save();
            }
            Session::flash('ingredients_mode', 'true');
            Session::flash('recipe_id', $data['recipe_id']);

            return Redirect::route('manage.recipes.create');
        }
        elseif(Input::get('finalize', 'false') == 'true')
        {
            return Redirect::route('manage.recipes.index');
        }


        $validator = Validator::make($data = Input::all(), Recipe::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $destinationPath_photo = '';
        $filename_photo = '';
        if (Input::hasFile('Image-Photos'))
        {
            $file_photo = Input::file('Image-Photos');
            $destinationPath_photo = self::DESTINATION_PATH_PHOTO;
            $extension_photo = $file_photo->getClientOriginalExtension();
            $filename_photo = str_random(11).".{$extension_photo}";
            $upload_success_photo = $file_photo->move(public_path() . '/' . $destinationPath_photo, $filename_photo);
        }


        $data = Input::all();
        $new_recipe = new Recipe;
        $new_recipe->product_id = $data['product'];
        $new_recipe->recipe_name = $data['recipe'];
        $new_recipe->prep_time = $data['prep_time'];
        $new_recipe->dish_by = $data['dish'];
        $new_recipe->serving_size = $data['serving'];
        $new_recipe->is_featured= Input::get('featured', 0);
        $new_recipe->badge1 = Input::get('badge1', 0);
        $new_recipe->badge2 = Input::get('badge2', 0);
        $new_recipe->badge3 = Input::get('badge3', 0);
        $new_recipe->photo = $destinationPath_photo.$filename_photo;
        $new_recipe->description = Input::get('description', '');
        $new_recipe->category = Input::get('category', '');
        $new_recipe->save();

        Session::flash('ingredients_mode', 'true');
        Session::flash('recipe_id', $new_recipe->id);

        return Redirect::route('manage.recipes.create');
    }

    /**
     * Display the specified recipe.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $recipe = Recipe::findOrFail($id);
        $ingredients =  Ingredient::where('recipe_id',$id)->get();
        $procedures =  Procedure::where('recipe_id',$id)->get();
        return View::make('recipes.show', compact('recipe'))
        ->with('procedures', $procedures)
        ->with('ingredients', $ingredients);
    }

    /**
     * Show the form for editing the specified recipe.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $products = Product::lists('title', 'id');
        $recipe = Recipe::find($id);
        $categories = Category::where('name', '!=', 'All')->lists('name', 'name');

        return View::make('recipes.edit', compact('recipe'))
        ->with('categories', $categories)
        ->with('products', $products);
    }

    /**
     * Update the specified recipe in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if(Input::has('edit_ing_pro'))
        {
            Session::flash('ingredients_mode', 'true');
            Session::flash('recipe_id', $id);

            return Redirect::route('manage.recipes.create');
        }

        $recipe = Recipe::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Recipe::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $recipe->update($data);
        if (Input::hasFile('Image-Photos'))
        {
            $file_photo = Input::file('Image-Photos');
			$destinationPath_photo = self::DESTINATION_PATH_PHOTO;			
            $extension_photo = $file_photo->getClientOriginalExtension();
            $filename_photo = str_random(11).".{$extension_photo}";
            $upload_success_photo = $file_photo->move(public_path() . '/' . $destinationPath_photo, $filename_photo);

        $photo = Recipe::find($id);
        $photo->photo = $destinationPath_photo.$filename_photo;
        $photo->save();
        }
        else
        {
        }
        $photo = Recipe::find($id);
        $photo->is_featured= Input::get('is_featured', 0);
        $photo->badge1 = Input::get('badge1',0);
        $photo->badge2 = Input::get('badge2',0);
        $photo->badge3 = Input::get('badge3',0);
        $photo->description = Input::get('description', '');
        $photo->category = Input::get('category', '');
        $photo->save();

        return Redirect::route('manage.recipes.index');
    }

    /**
     * Remove the specified recipe from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Recipe::destroy($id);

        return Redirect::route('manage.recipes.index');
    }

    /**
     * Fetch Recipes columns using AJAX
     *
     */
    public function getDatatable()
    {
        return Datatable::collection(Recipe::all())
            ->showColumns('product_id', 'recipe_name', 'category', 'prep_time', 'dish_by', 'serving_size', 'is_featured', 'badge1','badge2', 'badge3')
            ->addColumn('product_id', function($model) {

                return Recipe::productName($model->product_id);
            })
            ->addColumn('is_featured', function($model) {

                return $model->is_featured == 1 ? 'Yes': 'No';
            })
            ->addColumn('badge1', function($model) {

                return $model->badge1 == 1 ? 'Yes' : 'No';
            })
            ->addColumn('badge2', function($model) {

                return $model->badge2 == 1 ? 'Yes' : 'No';
            })
            ->addColumn('badge3', function($model) {

                return $model->badge3 == 1 ? 'Yes' : 'No';
            })
            ->addColumn('view', function($model) {
               return HTML::linkRoute('manage.recipes.show', 'View', $model->id, array('class' => 'btn btn-default btn-block'));
            })
            ->addColumn('edit', function($model) {
               return HTML::linkRoute('manage.recipes.edit', 'Edit', $model->id, array('class' => 'btn btn-default btn-block'));
            })
            ->addColumn('ingpro', function($model) {
               $a = Form::open(array('route' => array('manage.recipes.update',$model->id),'method' => 'put'))
               .Form::submit('Ingr./Proc.', array('name' => 'edit_ing_pro', 'class' => 'btn btn-warning'))
               .Form::close();
               return $a;
            })
			->addColumn('delete', function($model) {
                if (Auth::user()->isVisitor())
                {
                    return '';
                }
                else
                {
                    $form_open   = Form::open(array('route' => array('manage.recipes.destroy', $model->id), 'method' => 'delete'));
                    $form_button = Form::submit('Delete', array('class' => 'btn btn-danger'));
                    $form_close  = Form::close();

                    $button = '<!-- Button trigger modal -->
                    <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#myModal'.$model->id.'">
                      Delete
                    </button>';
                    $modal = '<!-- Modal -->
                    <div class="modal fade" id="myModal'.$model->id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Delete confirmation</h4>
                          </div>
                          <div class="modal-body">
                            Are you sure you want to delete?
                          </div>
                          <div class="modal-footer">
                            '.$form_open.$form_button.$form_close.'
                          </div>
                        </div>
                      </div>
                    </div>';
                    return $modal.$button;
                }
            })
            ->searchColumns('product_id', 'recipe_name', 'category', 'prep_time', 'dish_by', 'serving_size', 'is_featured', 'badge1','badge2', 'badge3')
            ->orderColumns('product_id', 'recipe_name', 'category', 'prep_time', 'dish_by', 'serving_size', 'is_featured', 'badge1','badge2', 'badge3')
            ->make();
    }

}
