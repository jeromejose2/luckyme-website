<?php

class CMSController extends BaseController {

    /**
     * Display the login page.
     * GET /cms
     *
     * @return Response
     */
    public function login()
    {
        return View::make('cms.login');
    }

    /**
     * Accepts the post request for login
     * of user in CMS
     *
     */
    public function userLogin()
    {
        $user_credentials['email']    = Input::get('email');
        $user_credentials['password'] = Input::get('password');

        //sets the remember_me variable
        if (Input::has('remember'))
        {
            $remember_me = true;
        }
        else
        {
            $remember_me = false;
        }

        if ($errors = User::loginIsInvalid($user_credentials))
        {
            return Redirect::route('manage.login')->withInput()->withErrors($errors);
        }

        if (Auth::attempt(array(
            'email'    => $user_credentials['email'],
            'password' => $user_credentials['password']), $remember_me))
        {
            return Redirect::route('manage.home');
        }
        else
        {
            return Redirect::route('manage.login')->withInput()->withErrors(array('fail' => 'E-mail or Password is incorrect'));
        }
    }

    /**
     * Accepts the post request for logout
     * of user in CMS
     *
     */
    public function userLogout()
    {
        Session::clear();
        Auth::logout();

        return Redirect::route('manage.login');
    }

    /**
     * Directs user to home page
     *
     */
    public function home()
    {
        return View::make('cms.home');
    }

}