<?php

class ProceduresController extends \BaseController {

	/**
	 * Display a listing of procedures
	 *
	 * @return Response
	 */
	public function index()
	{
		$procedures = Procedure::all();

		return View::make('procedures.index', compact('procedures'));
	}

	/**
	 * Show the form for creating a new procedure
	 *
	 * @return Response
	 */
	public function create()
	{
		$recipe = Recipe::lists('recipe_name', 'id');
		return View::make('procedures.create')
		->with('recipes', $recipe);
	}

	/**
	 * Store a newly created procedure in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Procedure::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		
		$data = Input::all();
        $new_procedures = new Procedure;
        $new_procedures->recipe_id = $data['recipe'];
        $new_procedures->name = $data['name'];
        $new_procedures->item = $data['item'];
        $new_procedures->save();

		return Redirect::route('manage.procedures.index');
	}

	/**
	 * Display the specified procedure.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$procedure = Procedure::findOrFail($id);

		return View::make('procedures.show', compact('procedure'));
	}

	/**
	 * Show the form for editing the specified procedure.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$procedure = Procedure::find($id);
        $recipe = Recipe::find($procedure->recipe_id);
        if(Session::has('recipe_id'))
        {
            $data = Session::all();
            return View::make('procedures.edit', compact('procedure'))
            ->with('recipe', $recipe)
            ->with('procedures_mode', 'true')
            ->with('recipe_id', $data['recipe_id']);
        }
		return View::make('procedures.edit', compact('procedure'))
		->with('recipe', $recipe);
	}

	/**
	 * Update the specified procedure in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$procedure = Procedure::findOrFail($id);
        $data = Input::all();
        if(Input::has('recipe_id'))
        {
            $recipe_id = $data['recipe_id'];
            unset($data['recipe_id']);
        }
		$validator = Validator::make($data, Procedure::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$procedure->update($data);

        if(isset($recipe_id))
        {
            return Redirect::route('manage.recipes.create')
                        ->with('ingredients_mode', 'true')
                        ->with('recipe_id', $recipe_id);
        }

		return Redirect::route('manage.procedures.index');
	}

	/**
	 * Remove the specified procedure from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Procedure::destroy($id);

		return Redirect::route('procedures.index');
	}

}
