<?php

class Category extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
           'name' => 'required|unique:categories'
	];

	// Don't forget to fill this array
	protected $fillable = ['name'];

}