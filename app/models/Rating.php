<?php

class Rating extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

    public static function addRating($recipe, $rating, $ip)
    {
        $query = Rating::where('ip_address', $ip)
                        ->where('recipe_id', $recipe)
                        ->first();
        if(empty($query))
        {
            $new_rating = new Rating;
            $new_rating->recipe_id = $recipe;
            $new_rating->rating = $rating;
            $new_rating->ip_address = $ip;
            $new_rating->browser = BrowserDetect::detect()->browserFamily;
            $new_rating->save();
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function getAverage($recipe)
    {
        $average = Rating::where('recipe_id', $recipe)->avg('rating');
        return $average;
    }
	
	public static function getTotal($recipe)
    {
        $total = Rating::where('recipe_id', $recipe)->count();
        return $total;
    }

}