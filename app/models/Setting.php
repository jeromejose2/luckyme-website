<?php

class Setting extends Eloquent {

    /**
     * Sets the Validation Rules when creating settings
     *
     * @var array
     */
    public static $rules = array(
        'type'    => 'required|min:6',
        'content' => 'required'
    );

    /**
     * Fillable array
     *
     * @var array
     */
    protected $fillable = array('type', 'content');

    public static function bitly($url)
    {
        try
        {
            $long_url = urlencode($url);

            $bitly_login = 'webnamnam';
            $bitly_apikey = 'R_91db2d6b18744850a41bb96cf6156a0a';

            $bitly_response = json_decode(file_get_contents("http://api.bit.ly/v3/shorten?login={$bitly_login}&apiKey={$bitly_apikey}&longUrl={$long_url}&format=json"));

            $short_url = $bitly_response->data->url;
        }
        catch(Exception $exception)
        {
            return $url;
        }
        return $short_url;
    }

}