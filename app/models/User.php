<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cms_users';

    /**
     * Fillable array
     *
     */
    protected $fillable = array('email', 'password', 'username', 'name', 'access_level');

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    /**
     * Sets the Validation Rules when Logging In
     *
     * @var array
     */
    public static $loginRules = array(
        'email'    => 'required|email',
        'password' => 'required|alpha_dash|min:6'
    );

    /**
     * Sets the Validation Rules creating a User
     *
     * @var array
     */
    public static $rules = array(
        'email'                 => 'required|email|unique:cms_users',
        'name'                  => 'required|alpha_spaces|min:2',
        'username'              => 'required|min:2|unique:cms_users',
        'access_level'          => 'required',
        'password'              => 'required|alpha_dash|min:6|confirmed',
        'password_confirmation' => 'required|alpha_dash|min:6'
    );

    /**
     * Sets the Validation Rules updating a User
     *
     * @var array
     */
    public static $updateRules = array(
        'email'                 => 'required|email',
        'name'                  => 'required|alpha_spaces|min:2',
        'username'              => 'required|min:2',
        'access_level'          => 'required',
        'password'              => 'required|alpha_dash|min:6|confirmed',
        'password_confirmation' => 'required|alpha_dash|min:6'
    );

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    /**
     * Gets the Remember Token
     *
     * @return    string    $this->remember_token
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * Set the Remember Token
     *
     * @param    string    $value
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    /**
     * Get the Remember Token name
     *
     * @return    string    'remember_token'
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * Get the password and Hash it before saving to the database.
     *
     * @param     string    $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Checks if Guest User input invalid credentials
     *
     * @param     array     $credentials
     * @return    object    $validation
     */
    public static function loginIsInvalid($credentials)
    {
        $validation = Validator::make($credentials, self::$loginRules);

        if ($validation->fails())
        {
            return $validation;
        }
    }

    /**
     * Checks if user access_level is 'super admin'
     *
     * @return    bool
     */
    public function isSuperAdmin()
    {
        if ($this->access_level === 'super admin')
        {
            return true;
        }

        return false;
    }

    /**
     * Checks if user access_level is 'visitor'
     *
     * @return    bool
     */
    public function isVisitor()
    {
        if ($this->access_level === 'visitor')
        {
            return true;
        }

        return false;
    }

}
