<?php

class Product extends Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
        'description' => 'max:500'
	];

	// Don't forget to fill this array
	protected $fillable = ['title','description','url'];

}