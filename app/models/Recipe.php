<?php

class Recipe extends Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array

	protected $fillable = ['product_id','recipe_name','badge1','badge2','badge3','prep_time','dish_by','serving_size','is_featured','photo'];

	public static function productName ($id)
	{
		$product = Product::find($id);
		return $product->title;
	}
}

