<?php

class DearBossing extends Eloquent {

    /**
     * Sets the Validation Rules when creating dearbossing
     *
     * @var array
     */
    public static $rules = array(
        //'episode_id'    => 'required|unique:dear_bossing',
        'title'         => 'required|min:2|unique:dear_bossing',
        //'article'       => 'required|min:2',
        'youtube_url'   => 'required|url',
        //'email_address' => 'required|email',
        //'is_active'     => 'required|boolean'
    );

    /**
     * Sets the Validation Rules when updating dearbossing
     *
     * @var array
     */
    public static $updateRules = array(
        //'episode_id'    => 'required',
        'title'         => 'required|min:2',
        //'article'       => 'required|min:2',
        'youtube_url'   => 'required|url',
        //'email_address' => 'required|email',
        //'is_active'     => 'required|boolean'
    );

    /**
     * Fillable array
     *
     * @var array
     */
    protected $fillable = array(
        'episode_id', 'title', 'article',
        'youtube_url','email_address', 'is_active'
    );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dear_bossing';
}