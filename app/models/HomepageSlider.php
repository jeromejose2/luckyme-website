<?php

class HomepageSlider extends Eloquent {

    /**
     * Sets the Validation Rules when creating new record
     *
     * @var array
     */
    public static $rules = array(
        'slider_id'             => 'required|unique:homepage_sliders',
        'button1_url_copyline1' => 'required_without:bg_video_url',
        'button1_url_copyline2' => 'required_without:bg_video_url',
        'button2_url_copyline1' => 'required_without:bg_video_url',
        'button2_url_copyline2' => 'required_without:bg_video_url',
        'button1_url_bg'        => 'required_without:bg_video_url',
        'button2_url_bg'        => 'required_without:bg_video_url',
         );

    /**
     * Sets the Validation Rules when updating a record
     *
     * @var array
     */
    public static $updateRules = array(
        'slider_id'             => 'required',
        'button1_url_copyline1' => 'required_without:bg_video_url',
        'button1_url_copyline2' => 'required_without:bg_video_url',
        'button2_url_copyline1' => 'required_without:bg_video_url',
        'button2_url_copyline2' => 'required_without:bg_video_url',
        'button1_url_bg'        => 'required_without:bg_video_url',
        'button2_url_bg'        => 'required_without:bg_video_url',
         );

    /**
     * Fillable array
     *
     * @var array
     */
    protected $fillable = array(
        'slider_id', 'photo', 'button1_url_copyline1',
        'button1_url_copyline2', 'button2_url_copyline1',
        'button2_url_copyline2', 'button1_bg_url',
        'button2_bg_url', 'bg_video_url'
    );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'homepage_sliders';

    public static function getYouTubeID($url)
    {
        $match = [];
        $video_id = $url;
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
            $video_id = $match[1];
        }
        return $video_id;
    }

    public static function getYouTubeURLFromID($id)
    {
        return 'http://www.youtube.com/watch?v='.$id;
    }

}