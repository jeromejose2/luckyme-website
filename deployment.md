(Commands are for Linux/UBUNTU -- MAKE SURE USER HAS PERMISSION ON /var/www/ directory)
### Install Git
    sudo apt-get install git

### Get Composer
    
    cd /usr/local/bin
    php -r "readfile('https://getcomposer.org/installer');" | php
    sudo mv composer.phar composer

### Clone/PUSH the project to /var/www/ (sample directory)
    
    cd /var/www/ 
    git clone [repo link] # this will clone the repo inside www folder
    
    // when testing we use the 777 on Dev, adjust value for security
    sudo chmod 777 -R /var/www/namnam/app/storage

   **OR** push to the repo into the production server

### Setting up VHOST

When creating the *.conf of the site. Just make sure that the *DocumentRoot* is set to `/var/www/namnam/public`

### Setup for the APP

On `/app/config/app.php`, replace the following fields with the production server values:

 - `'url' => 'URL-OF-SITE',`
 - `'timezone' => 'UTC',`

On `/app/config/database.php`

    'mysql' => array(
        'driver'    => 'mysql',
	'host'      => 'localhost',
	'database'  => 'DB_NAME',
	'username'  => 'DB_USERNAME',
	'password'  => 'DB_PASSWORD',
	'charset'   => 'utf8',
	'collation' => 'utf8_unicode_ci',
	'prefix'    => '',
    ),


### Getting the Vendor packages 

cd /var/www/namnam
composer update

If there is a log error, just give read/write permisison to `/var/www/namnam/app/storage`

### Creating Database

create the database with the name you included in the previous `app/config/database.php`

### Build the Database

to build the schema of the database

cd /var/www/namnam
php artisan migrate --seed

### Additional Notes

- enable mod rewrite
- make sure php mcrypt extension is installed